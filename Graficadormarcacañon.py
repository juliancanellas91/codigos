#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 15:06:55 2019

@author: julian
"""
# SACAR CAJA DE LAS LEYENDAS
#DIRECCIONES CON LINEA EN VEZ DE PUNTOS
import glob


CABAINVIERNO = sorted(glob.glob('CABAINVIERNO*'))
CABAINVIERNO.append('SOLARINVIERNO.csv')
CABAVERANO = sorted(glob.glob('CABAVERANO*'))
CABAVERANO.append('SOLARVERANO.csv')
RAWSONINVIERNO = sorted(glob.glob('RAWSONINVIERNO*'))
RAWSONVERANO = sorted(glob.glob('RAWSONVERANO*'))
GHIVERANO = sorted(glob.glob('GHIVERANO*'))
GHIINVIERNO = sorted(glob.glob('GHIINVIERNO*'))
VIENTOVERANO = sorted(glob.glob('VIENTOVERANO*'))
VIENTOINVIERNO = sorted(glob.glob('VIENTOINVIERNO*'))
DIRECCIONVERANO = sorted(glob.glob('DIRECCIONVERANO*'))
DIRECCIONINVIERNO = sorted(glob.glob('DIRECCIONINVIERNO*'))


import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.dates import HourLocator, DateFormatter

#############################
def WDIR(row):        
        if row['V1']> 0:
            wdir=(180/math.pi)*math.atan(row['U1']*1.0/row['V1'])+180
        elif row['U1']<= 0 and row['V1']< 0:
            wdir=(180/math.pi)*math.atan(row['U1']*1.0/row['V1'])+0
        elif row['U1']> 0 and row['V1']< 0:
            wdir=(180/math.pi)*math.atan(row['U1']*1.0/row['V1'])+360    
        elif row['V1']== 0 and row['U1']< 0:
            wdir=90      
        elif row['V1']== 0 and row['U1']> 0:
            wdir=270          
        elif row['V1']== 0 and row['U1']== 0:
            wdir=0  
           
        return wdir
###############################
nombre = ["Conf. 1","Conf. 3","Conf. 2", "Conf. 4", "Conf. 6", "Conf. 5", "Solar"]
dias = [(0,143), (144,287), (288,431), (432,575), (576,719), (720,863), (864,1008)]


#####################CABA INVIERNO#############################

#TODA LA SEMANA
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in CABAINVIERNO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    ghi = data['ghi']
    for i in range(len(ghi)):
        if ghi[i]>10000:
            ghi[i]=ghi[i]/1000
    ax.plot(ghi, label=nombre[CABAINVIERNO.index(file)])
    ax.xaxis.set_major_locator(HourLocator(interval=24))
    ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
ghiobs = pd.read_csv(GHIINVIERNO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
ax.plot(ghiobs['GHI OBS'], 'k.', label="Datos SAVER-NET")
ax.xaxis.set_major_locator(HourLocator(interval=24))
ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=8)
#plt.xlabel(' ')
plt.ylabel('Radiación Global [W/m2]')
plt.savefig("Gráficos/GHIINVIERNO.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()

#23 DE JUNIO
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in CABAINVIERNO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    ghi = data['ghi']
    for i in range(len(ghi)):
        if ghi[i]>10000:
            ghi[i]=ghi[i]/1000
    ax.plot(ghi[780:850], label=nombre[CABAINVIERNO.index(file)])
    ax.xaxis.set_major_locator(HourLocator(interval=3))
    ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
ghiobs = pd.read_csv(GHIINVIERNO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
ax.plot(ghiobs['GHI OBS'][780:850], 'k.', label="Datos SAVER-NET")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=8)
#plt.xlabel(' ')
plt.ylabel('Radiación Global [W/m2]')
plt.ylim([0,700])
plt.savefig("Gráficos/GHIINVIERNOdia23.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()

# ERROR

semanainvierno=["06-18","06-19","06-20","06-21","06-22","06-23","06-24"]
aver = np.arange(len(semanainvierno))
errorpordia = []
for num in dias:
    asd = []
    ghiobs = pd.read_csv(GHIINVIERNO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    ghiobs = ghiobs['GHI OBS']
    ghiobs = ghiobs[num[0]:num[1]]
    ghiobs = ghiobs[ghiobs>0]
    for file in CABAINVIERNO:
        data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        ghi = data['ghi']
        for i in range(len(ghi)):
            if ghi[i]>10000:
                ghi[i]=ghi[i]/1000
        ghi = ghi[num[0]:num[1]]
        desv = ghi - ghiobs
        desvest = np.std(desv)
        asd.append(desvest)
    errorpordia.append(asd)                


errorporcorrida = []

for i in range(0,7):
    auxlist = []
    for j in range(0,7):
        auxlist.append(errorpordia[j][i])
    errorporcorrida.append(auxlist)


vector = np.array([0,2.5,5,7.5,10,12.5,15])
w = 0.3
fig = plt.figure(figsize=(10,3))
for i in range(0,7):
    ax = fig.add_subplot(111)
    ax.bar(vector+i*w-1,errorporcorrida[i], width=w, align ='center', label=nombre[i])
plt.xticks(vector, semanainvierno)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)
plt.ylabel('RMSE de la Radiación Global [W/m2]')
plt.ylim([0,350])
plt.savefig("Gráficos/GHIINVIERNORMSE.png",  bbox_inches = 'tight',
    pad_inches = 0)  
    


#############################CABA VERANO ####################################

# TODA LA SEMANA
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in CABAVERANO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    ghi = data['ghi']
    for i in range(len(ghi)):
        if ghi[i]>10000:
            ghi[i]=ghi[i]/1000
    #ghi = ghi[ghi>0]
    ax.plot(ghi, label=nombre[CABAVERANO.index(file)])
ghiobs = pd.read_csv(GHIVERANO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
ghiobs = ghiobs['GHI OBS']
#ghiobs = ghiobs[ghiobs>0]
ax.plot(ghiobs, 'k.', label="Datos SAVER-NET")
ax.xaxis.set_major_locator(HourLocator(interval=24))
ax.xaxis.set_major_formatter(DateFormatter("Dic %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=8)

#plt.xlabel(' ')
plt.ylabel('Radiación Global [W/m2]')
plt.savefig("Gráficos/GHIVERANO.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()


# 19 DE DICIEMBRE
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in CABAVERANO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    ghi = data['ghi']
    for i in range(len(ghi)):
        if ghi[i]>10000:
            ghi[i]=ghi[i]/1000
    ax.plot(ghi[190:290], label=nombre[CABAVERANO.index(file)])
ghiobs = pd.read_csv(GHIVERANO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
ax.plot(ghiobs['GHI OBS'][190:290], 'k.', label="Datos SAVER-NET")
ax.xaxis.set_major_locator(HourLocator(interval=3))
ax.xaxis.set_major_formatter(DateFormatter("Dic %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=8)

#plt.xlabel(' ')
plt.ylim([0,1200])
plt.ylabel('Radiación Global [W/m2]')
plt.savefig("Gráficos/GHIVERANOdia19.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()

# 24 DE DICIEMBRE
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in CABAVERANO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    ghi = data['ghi']
    for i in range(len(ghi)):
        if ghi[i]>10000:
            ghi[i]=ghi[i]/1000
    ax.plot(ghi[920:1000], label=nombre[CABAVERANO.index(file)])
ghiobs = pd.read_csv(GHIVERANO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
ax.plot(ghiobs['GHI OBS'][920:1000], 'k.', label="Datos SAVER-NET")
ax.xaxis.set_major_locator(HourLocator(interval=3))
ax.xaxis.set_major_formatter(DateFormatter("Dic %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=8)

#plt.xlabel(' ')
plt.ylim([0,1200])
plt.ylabel('Radiación Global [W/m2]')
plt.savefig("Gráficos/GHIVERANOdia24.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()

# ERROR
dias2 = [(70,128), (204,282), (353,420), (495,560), (631,704), (785,858), (927,994)]
#dias2 = [(338,406)]
semanaverano=["12-18","12-19","12-20","12-21","12-22","12-23","12-24"]
errorpordia = []
for num in dias2:
    asd = []
    ghiobs = pd.read_csv(GHIVERANO[0])
    ghiobs = ghiobs['GHI OBS']
    ghiobs = ghiobs[num[0]:num[1]]
  #  ghiobs = ghiobs[ghiobs>10]
    for file in CABAVERANO:
        data = pd.read_csv(file )
        ghi = data['ghi']
        for i in range(len(ghi)):
            if ghi[i]>10000:
                ghi[i]=ghi[i]/1000
        ghi = ghi[num[0]:num[1]]
        desv = ghi - ghiobs
        desvest = np.std(desv)
        asd.append(desvest)
    errorpordia.append(asd)                


errorporcorrida = []

for i in range(0,7):
    auxlist = []
    for j in range(0,7):
 #   for j in range(0,1):
        auxlist.append(errorpordia[j][i])
    errorporcorrida.append(auxlist)


vector = np.array([0,2.5,5,7.5,10,12.5,15])
w = 0.3
fig = plt.figure(figsize=(10,3))
for i in range(0,7):
    ax = fig.add_subplot(111)
    ax.bar(vector+i*w-1,errorporcorrida[i], width=w, align ='center', label=nombre[i])
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)
plt.xticks(vector, semanaverano)
plt.ylim([0,350])
plt.ylabel('RMSE de la Radiación Global [W/m2]')
plt.savefig("Gráficos/GHIVERANORMSE.png",  bbox_inches = 'tight',
    pad_inches = 0)  
 


################## RAWSON INVIERNO ###############################


# TODA LA SEMANA, VIENTO
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in RAWSONINVIERNO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    U1 = data['U1']
    V1 = data['V1']
    for i in range(len(U1)):
        if U1[i]>1000:
            U1[i]=U1[i]/1000
    for i in range(len(V1)):
        if V1[i]>1000:
            V1[i]=V1[i]/1000
    for i in range(len(U1)):
        if U1[i]<-1000:
            U1[i]=U1[i]/1000
    for i in range(len(V1)):
        if V1[i]<-1000:
            V1[i]=V1[i]/1000        
    SPD = np.sqrt(U1**2+V1**2)
    ax.plot(SPD, label=nombre[RAWSONINVIERNO.index(file)])
anem1 = pd.read_csv(VIENTOINVIERNO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
anem1 = anem1['An 1 ']
ax.plot(anem1, 'k.', label="Torre Meteorológica")
ax.xaxis.set_major_locator(HourLocator(interval=24))
ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)
#inicio=data['0'][0]
#fin=data['0'][100]
#plt.axvspan(inicio,fin, alpha=0.1, color='b')
#plt.xlabel(' ')
plt.ylim([0,20])
#plt.xlim([0,1000])
plt.ylabel('Intensidad de Viento [m/s]')
plt.savefig("Gráficos/VIENTOINVIERNO.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()

#TODA LA SEMANA, DIRECCIÓN

fig = plt.figure(figsize=(10,3))
#fig.set_size_inches(6, 3, forward=True)
ax = fig.add_subplot(1,1,1)
for file in RAWSONINVIERNO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    U1 = data['U1']
    V1 = data['V1']
    data['DIR'] = data.apply(lambda row: WDIR(row), axis=1)
    DIR = data['DIR']
    ax.plot(DIR, label=nombre[RAWSONINVIERNO.index(file)])
anem1 = pd.read_csv(DIRECCIONINVIERNO[0], index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
anem1 = anem1['DIR']
ax.plot(anem1, 'k.', label="Torre Meteorológica")
ax.xaxis.set_major_locator(HourLocator(interval=24))
ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)
inicio=90
fin=190
plt.axhspan(inicio,fin, alpha=0.1, color='r')
inicio=0
fin=10
plt.axhspan(inicio,fin, alpha=0.1, color='r')
inicio=350
fin=360
plt.axhspan(inicio,fin, alpha=0.1, color='r')
#plt.xlabel(' ')
plt.ylim([0,360])
plt.yticks(np.array([0,90,180,270,360]))
#plt.xlim([0,1000])
plt.ylabel('Dirección de Viento [grados]')
plt.savefig("Gráficos/DIRECCIONINVIERNO.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()


#ERROR
semanainvierno=["06-18","06-19","06-20","06-21","06-22","06-23","06-24"]
errorpordia = []
for num in dias:
    asd = []
    anem = pd.read_csv(VIENTOINVIERNO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    anem = anem['An 1 ']
    anem = anem[num[0]:num[1]]
    for file in RAWSONINVIERNO:
        data = pd.read_csv(file , index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        U1 = data['U1']
        V1 = data['V1']
        for i in range(len(U1)):
            if U1[i]>1000:
                U1[i]=U1[i]/1000
        for i in range(len(V1)):
            if V1[i]>1000:
                V1[i]=V1[i]/1000
        for i in range(len(U1)):
            if U1[i]<-1000:
                U1[i]=U1[i]/1000
        for i in range(len(V1)):
            if V1[i]<-1000:
                V1[i]=V1[i]/1000
        SPD = np.sqrt(U1**2+V1**2)
        SPD = SPD[num[0]:num[1]]
        desv = SPD - anem
        desvest = np.std(desv)
        asd.append(desvest)
    errorpordia.append(asd)                


errorporcorrida = []

for i in range(0,6):
    auxlist = []
    for j in range(0,7):
        auxlist.append(errorpordia[j][i])
    errorporcorrida.append(auxlist)


vector = np.array([0,2.5,5,7.5,10,12.5,15])
w = 0.3
fig = plt.figure(figsize=(10,3))
for i in range(0,6):
    ax = fig.add_subplot(111)
    ax.bar(vector+i*w-1,errorporcorrida[i], width=w, align ='center', label=nombre[i])
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=6)
plt.xticks(vector, semanainvierno)
plt.ylim([0,10])
plt.ylabel('RMSE de la Intensidad de Viento [m/s]')
plt.savefig("Gráficos/VIENTOINVIERNORMSE.png",  bbox_inches = 'tight',
    pad_inches = 0)  
    

#DIA 23

fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in RAWSONINVIERNO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    U1 = data['U1']
    V1 = data['V1']
    for i in range(len(U1)):
        if U1[i]>1000:
            U1[i]=U1[i]/1000
    for i in range(len(V1)):
        if V1[i]>1000:
            V1[i]=V1[i]/1000
    for i in range(len(U1)):
        if U1[i]<-1000:
            U1[i]=U1[i]/1000
    for i in range(len(V1)):
        if V1[i]<-1000:
            V1[i]=V1[i]/1000
    SPD = np.sqrt(U1**2+V1**2)
    ax.plot(SPD[720:863], label=nombre[RAWSONINVIERNO.index(file)])
anem1 = pd.read_csv(VIENTOINVIERNO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
anem1 = anem1['An 1 ']
ax.plot(anem1[720:863], 'k.', label="Torre Meteorológica")
ax.xaxis.set_major_locator(HourLocator(interval=6))
ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)

#plt.xlabel(' ')
plt.ylim([0,20])
#plt.xlim([720,863])
plt.ylabel('Intensidad de Viento [m/s]')
plt.savefig("Gráficos/VIENTOINVIERNOdia23.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()


################## RAWSON VERANO ###############################


# TODA LA SEMANA, VIENTO
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in RAWSONVERANO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    U1 = data['U1']
    for i in range(len(U1)):
        if U1[i]>1000:
            U1[i]=U1[i]/1000
    V1 = data['V1']
    for i in range(len(V1)):
        if V1[i]>1000:
            V1[i]=V1[i]/1000
    data['SPD']= data.apply(lambda row: math.sqrt(row.U1**2 + row.V1**2), axis = 1)
    SPD = data['SPD']
    ax.plot(SPD, label=nombre[RAWSONVERANO.index(file)])
anem1 = pd.read_csv(VIENTOVERANO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
anem1 = anem1['An. 1']
ax.plot(anem1, 'k.', label="Torre Meteorológica")
ax.xaxis.set_major_locator(HourLocator(interval=24))
ax.xaxis.set_major_formatter(DateFormatter("Dic %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)

#plt.xlabel(' ')
plt.ylim([0,20])
#plt.xlim([0,1000])
plt.ylabel('Intensidad de Viento [m/s]')
plt.savefig("Gráficos/VIENTOVERANO.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()


#TODA LA SEMANA, DIRECCIÓN

fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in RAWSONVERANO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    U1 = data['U1']
    for i in range(len(U1)):
        if U1[i]>1000:
            U1[i]=U1[i]/1000
    V1 = data['V1']
    for i in range(len(V1)):
        if V1[i]>1000:
            V1[i]=V1[i]/1000
    data['DIR'] = data.apply(lambda row: WDIR(row), axis=1)
    DIR = data['DIR']
    ax.plot(DIR, label=nombre[RAWSONVERANO.index(file)])
anem1 = pd.read_csv(DIRECCIONVERANO[0], index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
anem1 = anem1['DIR']
inicio=90
fin=190
plt.axhspan(inicio,fin, alpha=0.1, color='r')
inicio=0
fin=10
plt.axhspan(inicio,fin, alpha=0.1, color='r')
inicio=350
fin=360
plt.axhspan(inicio,fin, alpha=0.1, color='r')
ax.plot(anem1, 'k.', label="Torre Meteorológica")
ax.xaxis.set_major_locator(HourLocator(interval=24))
ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)

#plt.xlabel(' ')
plt.ylim([0,360])
plt.yticks(np.array([0,90,180,270,360]))
#plt.xlim([0,1000])
plt.ylabel('Dirección de Viento [grados]')
plt.savefig("Gráficos/DIRECCIONVERANO.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()

#ERROR
semanaverano=["12-18","12-19","12-20","12-21","12-22","12-23","12-24"]
errorpordia = []
for num in dias:
    asd = []
    anem = pd.read_csv(VIENTOVERANO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    anem = anem['An. 1']
    anem = anem[num[0]:num[1]]
    for file in RAWSONVERANO:
        data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        U1 = data['U1']
        V1 = data['V1']
        for i in range(len(U1)):
            if U1[i]>1000:
                U1[i]=U1[i]/1000
        for i in range(len(V1)):
            if V1[i]>1000:
                V1[i]=V1[i]/1000
        data['SPD']= data.apply(lambda row: math.sqrt(row.U1**2 + row.V1**2), axis = 1)
        SPD = data['SPD']
        desv = SPD - anem
        desvest = np.std(desv)
        asd.append(desvest)
    errorpordia.append(asd)                


errorporcorrida = []

for i in range(0,6):
    auxlist = []
    for j in range(0,7):
        auxlist.append(errorpordia[j][i])
    errorporcorrida.append(auxlist)


vector = np.array([0,2.5,5,7.5,10,12.5,15])
w = 0.3
fig = plt.figure(figsize=(10,3))
for i in range(0,6):
    ax = fig.add_subplot(111)
    ax.bar(vector+i*w-1,errorporcorrida[i], width=w, align ='center', label=nombre[i])
plt.xticks(vector, semanaverano)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)
plt.ylim([0,6])
plt.ylabel('RMSE de la Intensidad de Viento [m/s]')
plt.savefig("Gráficos/VIENTOVERANORMSE.png",  bbox_inches = 'tight',
    pad_inches = 0)  
    
#DIA 22

fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)
for file in RAWSONVERANO:
    data = pd.read_csv(file, index_col='0', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    U1 = data['U1']
    V1 = data['V1']
    for i in range(len(U1)):
        if U1[i]>1000:
            U1[i]=U1[i]/1000
    for i in range(len(V1)):
        if V1[i]>1000:
            V1[i]=V1[i]/1000
    SPD = np.sqrt(U1**2+V1**2)
    ax.plot(SPD[576:719], label=nombre[RAWSONVERANO.index(file)])
anem1 = pd.read_csv(VIENTOVERANO[0], index_col='Hora', date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
anem1 = anem1['An. 1']
ax.plot(anem1[576:719], 'k.', label="Torre Meteorológica")
ax.xaxis.set_major_locator(HourLocator(interval=6))
ax.xaxis.set_major_formatter(DateFormatter("Jun %d - %H:%M"))
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), shadow=True, ncol=7)

#plt.xlabel(' ')
plt.ylim([0,20])
#plt.xlim([576,719])
plt.ylabel('Intensidad de Viento [m/s]')
plt.savefig("Gráficos/VIENTOVERANOdia22.png",  bbox_inches = 'tight',
    pad_inches = 0)
plt.show()






















