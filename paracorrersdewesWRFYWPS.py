import os

os.system('module load git curl cmake gcc/6.4.0')

os.system('. /nfs/home/jcanellas/spack/share/spack/setup-env.sh')
os.system('spack compiler find')
os.system('spack env activate gsiULTIMAPORFAVOR')
os.system('export PATH="$(spack location -i mpich)/bin:${PATH}"')
os.system('export PATH="$(spack location -i netcdf-c)/bin:${PATH}"')
os.system('export PATH="$(spack location -i netcdf-fortran)/bin:${PATH}"')
os.system('export HDF5=$(spack location -i hdf5)')
os.system('export NETCDF=$(spack location -i netcdf-fortran)')
os.system('export NETCDF_C=$(spack location -i netcdf-c)')
os.system('export ZLIB=$(spack location -i zlib)')
os.system('export LD_LIBRARY_PATH="${ZLIB}/lib:${HDF5}/lib:${NETCDF}/lib:${LD_LIBRARY_PATH}"')
os.system('export LD_LIBRARY_PATH="$(spack location -i libpng)/lib/:$LD_LIBRARY_PATH"')
os.system('export JASPER_ROOT=$(spack location -i jasper)')
os.system('export JASPERLIB="${JASPER_ROOT}/lib64"')
os.system('export JASPERINC="${JASPER_ROOT}/include"')
os.system('export LIBPNG="$(spack location -i libpng)"')
os.system('export LDFLAGS="-L$ZLIB/lib -L$JASPERLIB -L$LIBPNG/lib"')
os.system('export CPPFLAGS="-I$ZLIB/include -I$JASPERINC -I$LIBPNG/include"')
os.system('export FC="gfortran"')
os.system('export FCFLAGS=-m64')
os.system('export F77="gfortran"')
os.system('export FFLAGS="-m64"')
os.system('export CC="/nfs/soft/r6/gcc/6.4.0/bin/gcc"')
os.system('export CXX="/nfs/soft/r6/gcc/6.4.0/bin/g++"')
os.system('ulimit -s unlimited')
os.system('export WRF_EM_CORE=1')
os.system('export WRFIO_NCD_LARGE_FILE_SUPPORT=1')

os.system('ln -sf ${NETCDF_C}/include/* ${NETCDF}/include/')
os.system('ln -sf ${NETCDF_C}/lib/* ${NETCDF}/lib/')
os.system('ln -sf ${NETCDF_C}/lib/pkgconfig/* ${NETCDF}/lib/pkgconfig')




anos = [2015,2017]
meses = ["03","06","09",12]

os.system('cp WPS/namelistAUXILIAR.wps WPS/namelist.wps')
os.system('cp WRF/test/em_real/namelist.inputAUXILIAR WRF/test/em_real/namelist.input')

for i in anos:
	os.system('sed "s/start_date = 2015-12-01_00:00:00/start_date = '+str(i)+'-12-01_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
	os.system('mv WPS/namelist.wps2 WPS/namelist.wps')
        os.system('sed "s/end_date   = 2015-12-30_00:00:00/end_date   = '+str(i)+'-12-30_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
        os.system('mv WPS/namelist.wps2 WPS/namelist.wps')
	os.system('sed "s/start_year                          = 2015,/start_year                          = '+str(i)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
	os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')
        os.system('sed "s/end_year                            = 2015,/end_year                            = '+str(i)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
        os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')
	for j in meses:
		if j == "03":
			os.system('sed "s/start_date = '+str(i)+'-12-01_00:00:00/start_date = '+str(i)+'-'+str(j)+'-01_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
			os.system('mv WPS/namelist.wps2 WPS/namelist.wps')
                        os.system('sed "s/end_date   = '+str(i)+'-12-30_00:00:00/end_date   = '+str(i)+'-'+str(j)+'-30_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
                        os.system('mv WPS/namelist.wps2 WPS/namelist.wps')
		elif j == 12:
			os.system('sed "s/start_date = '+str(i)+'-09-01_00:00:00/start_date = '+str(i)+'-'+str(j)+'-01_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
                        os.system('mv WPS/namelist.wps2 WPS/namelist.wps')
                        os.system('sed "s/end_date   = '+str(i)+'-09-30_00:00:00/end_date   = '+str(i)+'-'+str(j)+'-30_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
                        os.system('mv WPS/namelist.wps2 WPS/namelist.wps')

		else:
			os.system('sed "s/start_date = '+str(i)+'-0'+str(int(j)-3)+'-01_00:00:00/start_date = '+str(i)+'-'+str(j)+'-01_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
                        os.system('mv WPS/namelist.wps2 WPS/namelist.wps')
                        os.system('sed "s/end_date   = '+str(i)+'-0'+str(int(j)-3)+'-30_00:00:00/end_date   = '+str(i)+'-'+str(j)+'-30_00:00:00/" WPS/namelist.wps > WPS/namelist.wps2')
                        os.system('mv WPS/namelist.wps2 WPS/namelist.wps')
		

		if j == "03":
                         os.system('sed "s/start_month                         = 12,/start_month                         = '+str(j)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
                         os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')
                         os.system('sed "s/end_month                           = 12,/end_month                           = '+str(j)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
                         os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')

                elif j == 12:
                         os.system('sed "s/start_month                         = 09,/start_month                         = '+str(j)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
                         os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')
                         os.system('sed "s/end_month                           = 09,/end_month                           = '+str(j)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
                         os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')

                else:
                         os.system('sed "s/start_month                         = 0'+str(int(j)-3)+',/start_month                         = '+str(j)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
                         os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')
                         os.system('sed "s/end_month                           = 0'+str(int(j)-3)+',/end_month                           = '+str(j)+',/" WRF/test/em_real/namelist.input > WRF/test/em_real/namelist.input2')
                         os.system('mv WRF/test/em_real/namelist.input2 WRF/test/em_real/namelist.input')

		os.system('./WPS/link_grib.csh /nfs/home/jcanellas/SDEWES/GFS/gfsanl_4_'+str(i)+str(j))

		os.system('cp WPS/namelist.wps .')
		os.system('cp WPS/Vtable .')
		os.system('cp -r WPS/metgrid .')

		os.system('./WPS/ungrib.exe')

		os.system('./WPS/metgrid.exe')
		os.system('rm GRI* FI*')

		os.system('cp WRF/test/em_real/namelist.input .')
		os.system('./WRF/test/em_real/real.exe')
		os.system('rm met_em*')

		os.system('mpirun -np 16 ./WRF/test/em_real/wrf.exe')

#		os.system('mkdir /nfs/home/jcanellas/SDEWES/out-'+str(i)+'-'+str(j)+' ')

		os.system('mv wrfo* /nfs/home/jcanellas/SDEWES/out-'+str(i)+'-'+str(j)+' ')
#		os.system('rm namelist.* wrfr* *.log rsl.* GRIB* Vtable')
