import this
import pandas as pd
from matplotlib import pyplot as plt
from glob import glob
import numpy as np


rmse = []
#max = []
#min = []
ano='2017'
dias = np.arange(1,32)
mes='Diciembre'
for dia in dias:
    del_mes = glob('./*'+ ano + '_' + mes + '*')
    del_mes.sort()

    rmse_diario = []
#    max_diario = []
#    min_diario = []
    gfs = pd.read_csv(del_mes[1],header= None, index_col=0, parse_dates=[0],
                            date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    gfs = gfs.tz_localize('UTC').tz_convert('UTC')
    gfs = gfs[gfs[1] != 0]
    obs = pd.read_csv(del_mes[0],header= None, index_col=0, parse_dates=[0],
                            date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    obs = obs.tz_localize('America/Argentina/Buenos_Aires').tz_convert('UTC')
    obs = obs[obs[1] != 0]
    #obs = obs.resample('1D').mean()
    for sims in del_mes[2:]:
        sim = pd.read_csv(sims, index_col=0,
                            parse_dates=[0], date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S+00:00'))
        sim = sim.tz_localize('UTC').tz_convert('UTC')
        sim = sim[sim['Mean'] != 0]
        #sim = sim.resample('1D').mean()
        daily_rmse = ((sim['Mean']-obs[1])**2).resample('1D').mean()**.5
        rmse_diario.append(daily_rmse[[times == dia for times in daily_rmse.index.day]])
        #max_diario.append(daily_rmse.max())
        #min_diario.append(daily_rmse.min())
    daily_rmse = ((gfs[1]-obs[1])**2).resample('1D').mean()**.5
    rmse_diario.append(daily_rmse[[times == dia for times in daily_rmse.index.day]])
    #max_diario.append(daily_rmse.max())
    #min_diario.append(daily_rmse.min())
    rmse.append(rmse_diario)
#    max.append(max_mensual)
#    min.append(min_mensual)


n_groups = len(dias)
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.1
opacity = 0.8

# simus = []
# for i in range(2,len(del_mes)):
#     simus.append(del_mes[i].rsplit('/')[1].rsplit('_')[0])
# simus.append(del_mes[1].rsplit('/')[1].rsplit('_')[0])

per_sim = []
for i in range(len(rmse[0])):
    per_sim.append([])
    for j in range(len(dias)):
        per_sim[i].append(rmse[j][i])

for i in range(len(per_sim)):
    for j in range(len(per_sim[0])):
        per_sim[i][j] = per_sim[i][j][0]


for i in range(len(per_sim)):
    bar_mensual = plt.bar(index + i*bar_width, per_sim[i], bar_width, alpha=opacity)
    #bar_extreme = plt.bar(index+ i*bar_width, per_sim_max[i],0.02 , bottom= per_sim_min[i], alpha=opacity, color='k')
#months = ['march','june','september','december']
plt.xlabel('Day')
plt.ylabel('RMSE (W/m²)')
plt.title('Daily root mean squared errors for December 2017')
plt.xticks(index, dias)
plt.legend(loc=(1,0))
plt.tight_layout()
plt.savefig('RMSE Diciembre 2017 diario.png')
plt.show()




        #     plt.plot(sim['ghi'], label=sims.rsplit('/')[1].rsplit('_')[0])
        # plt.plot(obs,'ko',markersize=1, label="CITEDEF")
        # plt.legend(loc=(1,0))
        # plt.savefig('GHI comparada '+ mes + ' '+ ano+'.png')




    plt.xlabel('Month')
    plt.ylabel('RMSE (W/m²)')
    plt.title('Monthly mean of daily root mean squared errors for year ' + ano)
