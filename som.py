import numpy as np
import xarray as xr
import netCDF4 as nc
import matplotlib.pyplot as plt
from minisom import MiniSom
from scipy.cluster.vq import kmeans
from scipy.stats import ks_2samp
import sys
import os
import math


def constant_decay(learning_rate, t, max_iter):
    """
    Constant decay function
    """
    return learning_rate

def arc_length(x, y):
    npts = len(x)
    arc = 0
    for k in range(1, npts):
        arc = arc + np.sqrt((x[k] - x[k-1])**2 + (y[k] - y[k-1])**2)
    return arc


ds = xr.load_dataset( 'adaptor.mars.internal-1630506188.6152434-3670-1-f238783b-87fb-4df4-90cb-99d54f8cb774.grib', engine='cfgrib')
z = ds['z'][:,1,:,:]

neuron_len = 20
lat = 41
lon = 61
rows = neuron_len
columns = neuron_len
num_iteration = 14608
sigma = 19
learning_rate = 0.005
times = 1

data = np.array(z)
data = np.reshape(data,(num_iteration,lat*lon))
data = np.nan_to_num(data)

k_means, error = kmeans(data,neuron_len**2)

som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
for i in range(neuron_len**2):
    som._weights[i//neuron_len][i%neuron_len] = data[np.random.randint(1,num_iteration)]

som.train(data, num_iteration*times)
pesos = np.zeros((neuron_len,neuron_len,lat,lon))
#devuelvo los pesos a (time,lat,lon)
for i in range(len(pesos)):
    for j in range(len(pesos[0])):
        k = 0
        while k < len(som.get_weights()[0][0]):
            pesos[i][j][k//lon][k%lon]=som.get_weights()[i][j][k]
            k += 1
fig, axs = plt.subplots(neuron_len, neuron_len, figsize=(20, 20))
for i in range(0, neuron_len):
    for j in range(0, neuron_len):
        axs[i][j].contour(pesos[i][j],colors='black')
        axs[i][j].imshow(pesos[i][j])
plt.show()

def DME(data,weights):

    dim = len(data[0,:])

    dme = 0

    for i in range(dim):

        result, pvalue = ks_2samp(data[:,i],weights[:,i])

        if 0.005 > pvalue:     

            dme += 1

    return dme/dim


def experiment(neuron_len,learning_rate_init,max_its):
    sigma_init = neuron_len - 1
    k_means , error = kmeans(data,neuron_len**2) 
    os.system("mkdir "+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its))
    stdoutOrigin=sys.stdout 
    sys.stdout = open("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/out.log", "w")
    som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma_init, learning_rate_init)
    for i in range(neuron_len**2):
        som._weights[i//neuron_len][i%neuron_len] = k_means[i]
    print("Metrics for k_means initialization")
    print(som.quantization_error(data))
    print(som.topographic_error(data))
    print(DME(data,np.reshape(som.get_weights(),(neuron_len**2,2501))))
    som.train(data,int(num_iteration*.5))
    print("Metrics for first training")
    print(som.quantization_error(data))
    print(som.topographic_error(data))
    print(DME(data,np.reshape(som.get_weights(),(neuron_len**2,2501))))
    pesos = np.zeros((neuron_len,neuron_len,lat,lon))
    #devuelvo los pesos a (time,lat,lon)
    for i in range(len(pesos)):
        for j in range(len(pesos[0])):
            k = 0
            while k < len(som.get_weights()[0][0]):
                pesos[i][j][k//lon][k%lon]=som.get_weights()[i][j][k]
                k += 1
    fig, axs = plt.subplots(neuron_len, neuron_len, figsize=(20, 20))
    for i in range(0, neuron_len):
        for j in range(0, neuron_len):
            axs[i][j].contour(pesos[i][j],colors='black')
            axs[i][j].imshow(pesos[i][j])
    plt.savefig("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/first_training.png")
    plt.close()
    plt.imshow(som.activation_response(data))
    plt.colorbar()
    fig.tight_layout()
    plt.savefig("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/first_training_activationresponse.png")
    plt.close()
    plt.imshow(som.distance_map(scaling='mean'))
    plt.colorbar()
    plt.savefig("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/first_training_distancemap.png")
    plt.close()
    weights = som.get_weights()
    if neuron_len > 10:
        sigma = int(neuron_len/10)
    else:
        sigma = 1
    som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate_init/5)
    som._weights = weights
    som.train(data,int(num_iteration*max_its*.3))
    print("Metrics for second training")
    print(som.quantization_error(data))
    print(som.topographic_error(data))
    print(DME(data,np.reshape(som.get_weights(),(neuron_len**2,2501))))
    plt.imshow(som.activation_response(data))
    plt.colorbar()
    fig.tight_layout()
    plt.savefig("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/second_training_activationresponse.png")
    plt.close()
    som.train(data,int(num_iteration*max_its*.7))
    print("Metrics for third training")
    print(som.quantization_error(data))
    print(som.topographic_error(data))
    print(DME(data,np.reshape(som.get_weights(),(neuron_len**2,2501))))
    pesos = np.zeros((neuron_len,neuron_len,lat,lon))
    #devuelvo los pesos a (time,lat,lon)
    for i in range(len(pesos)):
        for j in range(len(pesos[0])):
            k = 0
            while k < len(som.get_weights()[0][0]):
                pesos[i][j][k//lon][k%lon]=som.get_weights()[i][j][k]
                k += 1
    fig, axs = plt.subplots(neuron_len, neuron_len, figsize=(20, 20))
    for i in range(0, neuron_len):
        for j in range(0, neuron_len):
            axs[i][j].contour(pesos[i][j],colors='black')
            axs[i][j].imshow(pesos[i][j])
    plt.savefig("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/third_training.png")
    plt.close()
    plt.imshow(som.activation_response(data))
    plt.colorbar()
    fig.tight_layout()
    plt.savefig("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/third_training_activationresponse.png")
    plt.close()
    plt.imshow(som.distance_map(scaling='mean'))
    plt.colorbar()
    plt.savefig("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/third_training_distancemap.png")
    plt.close()
    sys.stdout.close()
    sys.stdout=stdoutOrigin
    os.system("cat ./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+"/out.log")
    np.save("./"+str(neuron_len)+"_"+str(sigma_init)+"_"+str(learning_rate_init)+"_"+str(max_its)+'/weights',som._weights)
    return som


i = 0
while not pd.DatetimeIndex(ds['time'].values)[i].year == 2018:
    i += 1     
i
while not pd.DatetimeIndex(ds['time'].values)[i].year == 2019:
    i += 1 
i

winners = []
for i in range(len(data[5848:11687])):
    winners.append(som.winner(data[i]))
    


ax = plt.axes()
for i in range(len(winners[1:])):
    a = [winners[i-1][0],winners[i-1][1]]
    b = [winners[i][0],winners[i][1]]
    head_length = 0.1
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    vec_ab = [dx,dy]
    vec_ab_magnitude = math.sqrt(dx**2+dy**2)
    dx = dx / vec_ab_magnitude
    dy = dy / vec_ab_magnitude
    vec_ab_magnitude = vec_ab_magnitude - head_length
    ax.arrow(a[1], a[0], vec_ab_magnitude*dy, vec_ab_magnitude*dx, head_width=0.05, head_length=0.1, fc='lightblue', ec='black')


directions = np.zeros((rows,columns,3))
for i in range(len(winners[1:])):
    a = [winners[i-1][0],winners[i-1][1]]
    b = [winners[i][0],winners[i][1]]
    head_length = 0.1
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    vec_ab = [dx,dy]
    vec_ab_magnitude = math.sqrt(dx**2+dy**2)
    dx = dx / vec_ab_magnitude
    dy = dy / vec_ab_magnitude
    dx = np.nan_to_num(dx)
    dy = np.nan_to_num(dy)
    directions[a[0],a[1],0] += dx
    directions[a[0],a[1],1] += dy
    directions[a[0],a[1],2] += 1

new_directions = np.zeros((rows,columns,2))

for i in range(len(directions)):
    for j in range(len(directions[0])):
        new_directions[i,j,0] = directions[i,j,0]/directions[i,j,2]
        new_directions[i,j,1] = directions[i,j,1]/directions[i,j,2]

ax = plt.axes()
for i in range(len(new_directions)):
    for j in range(len(new_directions[0])):
        ax.arrow(j, i, new_directions[i,j,1], new_directions[i,j,0], head_width=0.05, head_length=0.1, fc='lightblue', ec='black')
        
        
wind_rose = np.zeros((rows,columns,8,3))
for i in range(len(winners[1:])):
    a = [winners[i-1][0],winners[i-1][1]]
    b = [winners[i][0],winners[i][1]]
    head_length = 0.1
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    vec_ab = [dx,dy]
    vec_ab_magnitude = math.sqrt(dx**2+dy**2)
    dx = dx / vec_ab_magnitude
    dy = dy / vec_ab_magnitude
    dx = np.nan_to_num(dx)
    dy = np.nan_to_num(dy)
    wind_rose[a[0],a[1],int(np.nan_to_num((math.atan(dy/dx)*180/math.pi + 180)//45)),0] += dx
    wind_rose[a[0],a[1],int(np.nan_to_num((math.atan(dy/dx)*180/math.pi + 180)//45)),1] += dy
    wind_rose[a[0],a[1],int(np.nan_to_num((math.atan(dy/dx)*180/math.pi + 180)//45)),2] += 1
    
new_directions = np.zeros((rows,columns,8,2))

for i in range(len(new_directions)):
    for j in range(len(new_directions[0])):
        for k in range(len(new_directions[0][0])):
            new_directions[i,j,k,0] = wind_rose[i,j,k,0]/wind_rose[i,j,k,2]
            new_directions[i,j,k,1] = wind_rose[i,j,k,1]/wind_rose[i,j,k,2]
  
ax = plt.axes()
for i in range(len(new_directions)):
    for j in range(len(new_directions[0])):
        for k in range(len(new_directions[0][0])):
            ax.arrow(j, i, new_directions[i,j,k,1]/5, new_directions[i,j,k,0]/5, head_width=0.01, head_length=0.05, fc='lightblue', ec='black')
            