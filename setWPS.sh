#!/bin/bash

WRFbuild="$1" #/home/aotero/WRFintel_old

ln -s $WRFbuild/WPS/geogrid/src/geogrid.exe

ln -s $WRFbuild/WPS/metgrid/src/metgrid.exe

ln -s $WRFbuild/WPS/ungrib/src/ungrib.exe

ln -s $WRFbuild/WPS/ungrib

ln -s $WRFbuild/WPS/geogrid

ln -s $WRFbuild/WPS/metgrid

ln -s $WRFbuild/WPS/link_grib.csh
