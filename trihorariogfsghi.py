import this
from glob import glob
from datetime import datetime

filelist = glob('./*/*/serieghigfs.txt')

def decididor(terna):
    if terna == '003':
        return 130
    if terna == '006':
        return 430


for files in filelist:
    ds = open(files, 'r')
    series = []
    for lines in ds:
        lines = lines.strip('\n')
        lines = lines.rsplit(' ')
        date = datetime.strptime(lines[0]+str(f'{int(lines[1])+ decididor(lines[2]):04}'),'%Y%m%d%H%M')
        series.append([date,lines[3]])
    series.sort()
    for i in range(len(series)):
        if series[i][0].hour % 2 == 0 and i >= 1:
            series[i][1] = 2*float(series[i][1])-float(series[i-1][1])
    output = open('GFS' + '_' + str(files).rsplit('/')[1] + '_' + str(files).rsplit('/')[2] + '_trihorario.csv', 'w+')
    for i in range(len(series)):
        output.write(str(series[i][0])+','+str(series[i][1])+'\n')
    output.close()
