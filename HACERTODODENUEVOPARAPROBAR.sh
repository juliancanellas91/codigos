#!/bin/bash -x

echo $PWD
mkdir PROBANDOTODODENUEVO
echo "copio..."
cp -r WRFV4/WPS PROBANDOTODODENUEVO/
cp -r WRFV4/WRF PROBANDOTODODENUEVO/
cp -r WRFV4/WRFPLUS PROBANDOTODODENUEVO/
cp -r WRFV4/WRFDA PROBANDOTODODENUEVO/
cp -r GSI PROBANDOTODODENUEVO/
echo "spack..."
. spack/share/spack/setup-env.sh

spack compiler find

module load git curl cmake gcc/6.4.0

spack env create probandotododenuevo
spack env activate probandotododenuevo

spack install netcdf-fortran ^mpich ^hdf5+hl+fortran %gcc@6.4.0
spack install jasper %gcc@6.4.0
spack install libpng %gcc@6.4.0
spack install openblas@0.3.9 %gcc@6.4.0
spack install zlib %gcc@6.4.0
echo "variables de entorno..."
export OPENBLAS=$(spack location -i openblas)
export PATH="$(spack location -i mpich)/bin:${PATH}"
export PATH="$(spack location -i netcdf-c)/bin:${PATH}"
export PATH="$(spack location -i netcdf-fortran)/bin:${PATH}"
export HDF5=$(spack location -i hdf5)
export NETCDF=$(spack location -i netcdf-fortran)
export NETCDF_C=$(spack location -i netcdf-c)
export ZLIB=$(spack location -i zlib)
export LD_LIBRARY_PATH="${ZLIB}/lib:${HDF5}/lib:${NETCDF}/lib:${LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH="$(spack location -i libpng)/lib/:$LD_LIBRARY_PATH"
export JASPER_ROOT=$(spack location -i jasper)
export JASPERLIB="${JASPER_ROOT}/lib64"
export JASPERINC="${JASPER_ROOT}/include"
export LDFLAGS="-L$ZLIB/lib -L$JASPERLIB -L$(spack location -i libpng)/lib"
export CPPFLAGS="-I$ZLIB/include -I$JASPERINC -I$(spack location -i libpng)/include"
export FC="gfortran"
export FCFLAGS=-m64
export F77="gfortran"
export FFLAGS="-m64"
export CC="/nfs/soft/r6/gcc/6.4.0/bin/gcc"
export CXX="/nfs/soft/r6/gcc/6.4.0/bin/g++"

ulimit -s unlimited
export WRF_EM_CORE=1
export WRFIO_NCD_LARGE_FILE_SUPPORT=1

ln -sf ${NETCDF_C}/include/* ${NETCDF}/include/
ln -sf ${NETCDF_C}/lib/* ${NETCDF}/lib/
ln -sf ${NETCDF_C}/lib/pkgconfig/* ${NETCDF}/lib/pkgconfig

cd PROBANDOTODODENUEVO

echo $PWD
echo "arranco"
#### Arranco con GSI ####

cd GSI
echo "GSI"
echo $PWD

rm -r *

cmake -DLAPACK_LIBRARIES=$OPENBLAS/lib/libopenblas.a -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_Fortran_COMPILER=gfortran ../../comGSIv3.7_EnKFv1.3 && make

ls -lsh

#### WRF ####

cd ../WRF
echo "WRF"
echo $PWD

./clean
./clean -a 

printf '34\n1\n' | ./configure

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wrf

./compile em_real 2>&1 | tee wrf_compile.log

ls -lsh main/*.exe

#### WPS ####

cd ../WPS
echo "WPS"
echo $PWD

./clean 
./clean -a

printf '1\n' | ./configure

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wps

./compile 2>&1 | tee wps_compile.log

ls -lsh ungrib/src/*.exe geogrid/src/*.exe metgrid/src/*.exe

#### WRFPLUS ####

cd ../WRFPLUS
echo "WRFPLUS"
echo $PWD 

./clean
./clean -a

printf '18\n' |./configure wrfplus

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wrf

./compile wrfplus 2>&1 | tee wrfplus_compile.log

export WRFPLUS_DIR=$PWD

ls -lsh main/*.exe

#### WRFDA ####

cd ../WRFDA
echo "WRFDA"
echo $PWD 

./clean
./clean -a

printf '18\n' | ./configure 4dvar

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wrf

./compile all_wrfvar 2>&1 | tee 4dvar_compile.log

ls -lsh var/build/*.exe var/obsproc/*.exe
