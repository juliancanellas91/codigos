#!/bin/bash -x
cd "/nfs/home/jcanellas/SDEWES 2020/2017"

for months in ./*/  ; do
	cd "$months"WPS
        echo "$PWD"
        ./link_grib.csh ../DATA/gfsanl_
        echo "DATA linked"
        ./ungrib.exe
        ./geogrid.exe
        ./metgrid.exe
        cd ../WRF/test/em_real5
	echo "$PWD"
        ln -sf ../../../WPS/met_em* .
        ./real.exe
	cd "../../../.."
	echo "$PWD"
done



