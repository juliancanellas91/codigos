from glob import glob
import pandas as pd
import os

#for sims in glob('./*/*/*/'):
#    os.system('cp Paraextraerunpardeseries.py ' + sims)


#os.system('bash corredor.sh')

for sims in glob('./*/*/*/'):
    ds = pd.read_csv(sims+'ExtraidasRadiacion.csv',  index_col='Unnamed: 0',
                        parse_dates=[0], date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    ghi = ds['ghi']
    #i = 0
    #while ghi.index.hour[i] != 3:
    #    i += 1
    #ghi = ghi[i:]
    # ghi.index = pd.to_datetime(ghi.index)
    i = 0
    while (ghi.index.hour[i] != 1) or (ghi.index.minute[i] != 30):
        i += 1
    j = len(ghi)-1
    while (ghi.index.hour[j] != 22) or (ghi.index.minute[j] != 30):
        j -=1
    ghi = ghi[i:j]
    ghi = ghi.resample('3H',loffset='1.5H').mean()
    ghi.to_csv('Simu' + str(sims).rsplit('/')[3] + '_' + str(sims).rsplit('/')[1]+ '_' + str(sims).rsplit('/')[2] + '_trihorario.csv')
    # ghi = ghi.plot()
    # ghi = ghi.get_figure()
    # ghi.savefig(str(sims).rsplit('/')[1]+str(sims).rsplit('/')[2]+str(sims).rsplit('/')[3] + 'trihorario.png')



ds['ghi'].plot()
ds['dni'].plot()
ds['dif'].plot()
