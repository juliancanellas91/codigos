# -*- coding: utf-8 -*-
#######################################################################
#Codigo para analalizar y extraer datos de reanalisis MERRA
#######################################################################
import math
import netCDF4
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from windrose import WindroseAxes
from windrose import WindAxes
import os
import glob
from mpl_toolkits.basemap import Basemap
import xarray as xr

########################################################################
# se crea la funcion ubicacion, que recibira latitudes y longitudes
# y devolvera puntos de grilla
def ubicacion(lat,long):
    ubicacion = []
    for i in range (len(lat)):
        for j in range (len(long)):
            punto= (lat[i],long[j])
            ubicacion.append(punto)

    return ubicacion
#########################################################################
# Se crea la funcion distanciaMinima(a) que calcula la distancia minima
#entre un punto dado y los puntos de grilla y devuelve el punto de grilla
# mas cercano al punto de interes

def distanciaMinima(a,b): #donde b es mi punto de interes
	dmin = 0
#Se evalua la distancia de todas las tuplas contra las otras
	for i in range (0,(len(a))):
          d=math.sqrt(((a[i][0]-b[0])**2)+((a[i][1]-b[1])**2))
#Si la distancia calculada es menor a la que hasta ese momento era la
#minima se retiene ese valor como dmin y se deja asentado en c
#las coordenadas cuya distancia es la menor
          if dmin == 0:
              dmin = d
          if d <= dmin:
              dmin = d
              c = a[i]
	return c
#########################################################################
# Se crea la funcion puntoMasCercano que se le ingresa un punto (longitud,
#latiud) y devuelve el punto de grilla mas cercano a esas coordenadas

def puntoMasCercano (lat,long,a):
    u= ubicacion(lat,long)
    c= distanciaMinima(u,a)
    for i in range (0,len(lat)):
        if lat[i] == c[0]:
            x=i
    for j in range (0,len(long)):
        if long[j] == c[1]:
            y=j

    return x,y
########################################################################
# Se crea la funcion viento, para obtener la intesidad y direccion
# del viento de los valores u y v
########################################################################
def viento(u,v):
    tiempo = [] # Se construye el vector tiempo
    spd = [] # Se construye el vector intensidad del viento
    wdir = [] # Se construye el vector direccion del viento

    for i in range (0,len(u)):
        tiempo.append(i)
        spd.append(math.sqrt(u[i]**2 + v[i]**2))

        if v[i]> 0:
            wdir.append((180/math.pi)*math.atan(u[i]*1.0/v[i])+180)
        elif u[i]<= 0 and v[i]< 0:
            wdir.append((180/math.pi)*math.atan(u[i]*1.0/v[i])+0)
        elif u[i]> 0 and v[i]< 0:
            wdir.append((180/math.pi)*math.atan(u[i]*1.0/v[i])+360)
        elif v[i]== 0 and u[i]< 0:
            wdir.append(90)
        elif v[i]== 0 and u[i]> 0:
            wdir.append(270)
        elif v[i]== 0 and u[i]== 0:
            wdir.append(0)

    return(tiempo,spd,wdir)


datalist = os.listdir("")

PRUEBA 1

filelist = glob.glob('*.nc4')
u = []
v = []
i = 0
j = 0
for fl in filelist:
    ds = xr.open_dataset(fl)
    u.append(ds['U50M'][:])
    v.append(ds['V50M'][:])

while i < len(u):
    while j < len(v):
        tiempo,spd,wdir=viento(u[i],v[j])
        ax = WindroseAxes.from_ax()
        ax.bar(wdir, spd, normed=True, opening=0.8, edgecolor='white')
        ax.set_legend()
        plt.title('Rosa de vientos MERRA')
        plt.savefig('Rosa de vientos MERRA.png')
        i = i+1
        j = j+1
            
PRUEBA2


PRUEBA3

filelist = glob.glob('*.nc4')
u = []
v = []
for fl in filelist:
    ds = xr.open_dataset(fl)
    u.append(ds['U50M'][:])
    v.append(ds['V50M'][:])
    
ax = WindroseAxes.from_ax()
for i in range(len(u)):
        tiempo,spd,wdir=viento(u[i],v[i])
        ax.bar(wdir, spd, nsector=32, bins=12, opening=0.8, edgecolor='white')
        print(i)
ax.set_yticks(np.arange(0, 25, step=5))
ax.set_yticklabels(np.arange(0,25, step=5))
ax.set_legend()
plt.title('Rosa de los vientos 50m para 60deg45min53seg W 37deg18min1seg S')
plt.savefig('Rosa de vientos MERRA 50m.png')



ax = WindAxes.from_ax()
bins = np.arange(0, 22, 1)
bins = bins[1:]
for i in range(len(u)):
    ax, params = ax.pdf(spd, bins=bins)
    wp1=[[params[1]],[params[3]]]
    etiquetas_fil= ('k', 'Viento medio')
    plt.table(cellText=wp1, rowLabels = etiquetas_fil, colWidths = [0.2], loc='upper right')
    plt.title('Weibull MERRA')
    plt.savefig('Weibull MERRA.png')
        
    #map = Basemap(projection='robin',lon_0=0,resolution='l',ax=ax)
    #cs = map.scatter(x,y,data)
    #fig.savefig("Probando.png")


















#######################################################################
# Se lee el archivo del reanalisis MERRA
#######################################################################
g = netCDF4.Dataset("/home/shaper/Documentos/DATOS/MERRA2_300.tavg1_2d_slv_Nx.20090102.nc4")
gu10m = g.variables['U10M']
U10M = gu10m[:]



latG= g.variables['lat'][:]
longG= g.variables['lon'][:]

PR1=(-43.35,-65.18) #####PUNTO DE INTERES

(x1,y1)=puntoMasCercano(latG,longG,PR1)


u=g.variables['U50M'][:,x1,y1]
v=g.variables['V50M'][:,x1,y1]


tiempo,spd,wdir=viento(u,v)


###############################################################################
# Salidas Graficas
###############################################################################


# Graficos de Rosa de vientos

ax = WindroseAxes.from_ax()
ax.bar(wdir, spd, normed=True, opening=0.8, edgecolor='white')
ax.set_legend()
plt.title('Rosa de vientos MERRA')
plt.savefig('Rosa de vientos MERRA.png')

# Graficos de histogramas y curvas de ajuste Weibull

ax = WindAxes.from_ax()
bins = np.arange(0, 22, 1)
bins = bins[1:]
ax, params = ax.pdf(spd, bins=bins)
wp1=[[params[1]],[params[3]]]
etiquetas_fil= ('k', 'Viento medio')
plt.table(cellText=wp1, rowLabels = etiquetas_fil, colWidths = [0.2], loc='upper right')
plt.title('Weibull MERRA')
plt.savefig('Weibull MERRA.png')
