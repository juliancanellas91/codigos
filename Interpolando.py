#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 15:14:45 2019

@author: julian
"""
import this
import matplotlib as plt
import cartopy as crt
import numpy as np
import xarray as xr
import xesmf as xe
import xgcm as xg
import scipy as sci
import gamap 
import netCDF4
import datetime
import pandas as pd
import mpl_toolkits
import math
from gamap import WhGrYlRd

ASSdic17 = xr.open_dataset("Documentos/WRFARW/Build_WRF/WPS_GEOG/PRUEBA/ASSdic17.nc")
AODdic17 = xr.open_dataset( "/home/julian/Documentos/WRFARW/Build_WRF/WPS_GEOG/EXTTAU/g4.subsetted.M2TMNXAER_5_12_4_TOTEXTTAU.20171201.180W_90S_180E_90N.nc")
ASSdic17['OMAERUVd_003_FinalAerosolSingleScattAlb500']
AODdic17['M2TMNXAER_5_12_4_TOTEXTTAU']


aver = xr.open_dataset("/home/julian/Documentos/WRFARW/Build_WRF/WRF/test/em_real/wrfout_d01_2017-12-11_06:00:00")

vectorlatitudDESTINO = aver['XLAT'][0,:,1].values

vectorlongitudDESTINO = aver['XLONG'][0,1,:].values

#estos dos me sirven para ver las coordenadas destino de mis datos. 

vectorlatitudPARTIDA = AODdic17['lat'][:].values

vectorlongitudPARTIDA = AODdic17['lon'][:].values

#estos dos son las coordenadas desde la que tengo que interpolar

metem = xr.open_dataset("Documentos/WRFARW/Build_WRF/WRF/test/em_real/met_em.d01.2017-12-11_06:00:00.nc")


min(vectorlatitudDESTINO)
max(vectorlatitudDESTINO)
min(vectorlongitudDESTINO)
max(vectorlongitudDESTINO)

#Necesito recortar los vectores de PARTIDA entre -46.5 y -19.5 
#y entre -46.5 y -69.5


# busco los extremos en términos de su índice
for i in range(0,361):
    if vectorlatitudPARTIDA[i] == -69.5:
        print(i)

#el extremo Sur del dominio es i = 43
#el extremo Norte del dominio es i = 71
#el extremo Oeste del dominio es i = 110
#el extremo Este del dominio es i = 134

AUXvectorlatPARTIDA = vectorlatitudPARTIDA[43:71]
   
AUXvectorlonPARTIDA = vectorlongitudPARTIDA[110:134]

#RegionInteresante = ASSdic17['OMAERUVd_003_FinalAerosolSingleScattAlb500'][43:71,110:134].values

RegionTotal = AODdic17['M2TMNXAER_5_12_4_TOTEXTTAU']


RegionInteresante = RegionTotal[43:71,110:134]


for lat in range(0,28):
    for lon in range(0,24):
        if math.isnan(RegionInteresante[lat,lon]):
            RegionInteresante[lat,lon] = 0
            


#Ahora ya tengo mis vectores recortados con la parte del globo que me interesa. Sólo queda interpolar
            
#Armo mi target grid
            
len(vectorlatitudDESTINO)

len(vectorlongitudDESTINO)
            
Target_Grid = {'lon': vectorlongitudDESTINO,
               'lat' : vectorlatitudDESTINO
               }
            
            
#Voy a trabajar sobre Region Interesante definida CON los NaN            

Interpolador_bilineal = xe.Regridder(RegionTotal,Target_Grid, method = 'bilinear')


RegionInterpolada = Interpolador_bilineal(RegionTotal)



RegionInteresante = RegionTotal[0,80:150,215:270]

RegionInteresante.plot(cmap=WhGrYlRd)
     #FUNCIONÓ!!!       
RegionInterpolada.plot(cmap=WhGrYlRd)        
RegionTotal.plot(cmap=WhGrYlRd)
          

RegionInterpolada.to_netcdf('/home/julian/Documentos/WRFSolar/A ver este AOD/Datos.nc')
  
            
# en realidad yo tengo que apuntar a emluar este archivo.


Robarte = nc.Dataset('/home/julian/Documentos/WRFSolar/wrfsolar-1.2.4/WRFV3/test/em_real/wrfbdy_d01DUPLICADO')

TusVariables = nc.Dataset('/home/julian/Documentos/WRFSolar/A ver este AOD/Datos.nc')

#  Tengo que estar parado en la carpeta A ver este AOD para hacer esto: 
with netCDF4.Dataset("wrfbdy_d01DUPLICADO") as src, netCDF4.Dataset("AOD550dic17OMIinterpolado.nc", "w") as dst:
    dst.setncatts(src.__dict__)
    for name, dimension in src.dimensions.items():
        dst.createDimension(name, (len(dimension) if not dimension.isunlimited() else None))
    for name, variable in TusVariables.variables.items():
        x = dst.createVariable(name, variable.datatype, variable.dimensions)
        dst[name][:] = TusVariables[name][:]
        dst[name].setncatts(TusVariables[name].__dict__)

#PROBAR LO SIGUIENTE EN ALGÚN FUTURO EN EL QUE NO ESTÉ TAN QUEMADO:
        #Me estuvo cagando la vida un error que decía :
        # Write to read no se qué
        #Cuestión que parece que el comando Dataset de nc tiene la opción de abrirlo con "w", que sería 
        #para escribirlo. Debería probar repetir TODO lo que hice con este script, pero agregando un "w" a TODO
        #lo que abro. 







ASSdic17 = nc.Dataset("/home/julian/Documentos/WRFARW/Build_WRF/WPS_GEOG/PRUEBA/ASSdic17.nc")



lon = ASSdic17.variables['lon'][:]
lat = ASSdic17.variables['lat'][:]

SSAdata = ASSdic17.variables['OMAERUVd_003_FinalAerosolSingleScattAlb500']



































# A partir de acá abajo robo cosas de Paraextraerunpardeseries.py

# vm = nc.MFDataset('/home/julian/Documentos/WRFARW/Build_WRF/WRF/test/em_real/wrfout_d03_2017-12-1*')

# latitude = -34.550

# longitude = -58.320

# La idea es tener un archivo netCDF abierto como vm para extraerle las variables de radiación en el
# data = vm


gridDistN = (ASSdic17['lat'][:].values - latitude)**2 
            

iLat, iLon = np.unravel_index(np.argmin(gridDist, axis=None),
                                      gridDist.shape)


#para cada punto de grilla, quiero definir una GridDistN GridDistS GridDistE GridDistW y buscar el mínimo en esa dirección.
#Así obtendría 4 puntos más cercanos en la dirección cardinal y los promediaría para asignar un valor de grilla. 







for latitude in vectorlatitudDESTINO:
    for longitude in vectorlongitudDESTINO:
        #para cada punto de DESTINO, quiero encontrar 4 puntos 
        for idx in vectorlatitudPARTIDA:
            if idx > latitude:
                gridDistN[idx] = (ASSdic17['lat'][:].values - latitude)**2 


