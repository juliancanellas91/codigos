import netCDF4 as nc
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import os
from wrf import extract_times

files = ['diff4doutRECORTADO','diff3doutRECORTADO']

for filename in files:

    ds = nc.Dataset('../Corridas/' + filename)

    level = 18 # 5300m

    variables = ['QCLOUD','QVAPOR','W']
    for var in variables:
        for time in range(109):

            map = Basemap(llcrnrlon=ds['XLONG'][time,50,:].min(),
                          llcrnrlat=ds['XLAT'][time,:,1].min(),
                          urcrnrlon=ds['XLONG'][time,50,:].max()-3,
                          urcrnrlat=ds['XLAT'][time,:,1].max(),
                          resolution = 'i',
                          projection = 'lcc',
                          lat_0 = ds['XLAT'][time,:,:].mean(),
                          lon_0 = ds['XLONG'][time,:,:].mean())

            map.drawcoastlines()
            map.drawcountries()

            parallels = np.arange(np.round(ds['XLAT'][time,:,1].min()),
                                  np.round(ds['XLAT'][time,:,1].max()),
                                  5.)
            # labels = [left,right,top,bottom]
            map.drawparallels(parallels,labels=[True,False,True,False])
            meridians = np.arange(np.round(ds['XLONG'][time,50,:].min()),
                                  np.round(ds['XLONG'][time,50,:].max()),
                                  5.)
            map.drawmeridians(meridians,labels=[True,False,False,True])

            x,y = map(ds['XLONG'][time,:,:], ds['XLAT'][time,:,:])
            map.contourf(x,
                         y,
                         ds[var][time,level,:,:])
            plt.clim(ds[var][:,level,:,:].min(),ds[var][:,level,:,:].max())
            # map.colorbar(location='right', label='Diferencia de contenido de agua \
                                 # líquida en 500 hPa (aprox)',size='2%')
            plt.colorbar()
            # plt.show()
            if var == variables[0]:
                title = 'Contenido de Agua Líquida'
            elif var == variables[1]:
                title = 'Contenido de Vapor'
            elif var == variables[2]:
                title = 'Velocidad Vertical'

            if filename == files[0]:
                exp = '4DVar - Control'
            elif filename == files[1]:
                exp = '3DVar - Control'

            plt.title('Diferencia en '+title+' '+exp+'\n'+\
                      str(extract_times(ds,time)))

            if time < 10:
                plt.savefig(var+'00'+str(time)+filename+'.png')
            elif time >= 10 and time < 100:
                plt.savefig(var+'0'+str(time)+filename+'.png')
            else:
                plt.savefig(var+str(time)+filename+'.png')
            plt.close()
        os.system('convert -delay 20 -loop 0 *.png '+ var + filename + '.gif')
        os.system('rm *.png')
