
from glob import glob
import pygrib as gr
import numpy as np

for years in glob('/home/julian/Documentos/Proyectos/SDEWES 2020/GFSghi/*/'):
    ano = str(years).rsplit('/')[7]
    for months in glob(years + '*/'):
        mes = str(months).rsplit('/')[8]
        filelist = glob(months + 'gfsanl*.grb2')
        output = open(months + 'serieghigfs.txt', 'w+')
        for files in filelist:
            ds = gr.open(files)
            a = str(ds()).rsplit(',')
            for line in a:
                if "Downward short-wave radiation flux" in line:
                    if ano == '2015':
                        ghi = ds()[284]
                    elif mes == 'Septiembre' or mes == 'Diciembre':
                        ghi = ds()[334]
                    elif mes == 'marzo' or mes == 'Junio':
                        ghi = ds()[333]
                    ghi = ghi.data()[0][249,603]
                    output.write(str(files).rsplit('_')[2] + ' ' + str(files).rsplit('_')[3] + ' ' + str(files).rsplit('_')[4].rsplit('.')[0] + ' ' + str(round(ghi)) + '\n')
        output.close()
