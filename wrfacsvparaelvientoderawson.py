#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 13:10:30 2019

@author: julian
"""

import matplotlib.pyplot as plt
import datetime
import pandas as pd
import numpy as np
import netCDF4 as nc
import glob

filelist = glob.glob('wrfout_d01*')

latitude = -44.059
longitude = -67.017

iLat = 76 #SIEMPRE CHEQUEAR ESTO!
iLon = 84


vm = nc.MFDataset(filelist)
data = vm
time = []
for t in data.variables['Times'][:]:
            a = t.tostring().decode().split('_')
            c = a[0].split('-') + a[1].split(':')
            time.append(datetime.datetime(*list(map(int, c))))
timepd = pd.DatetimeIndex(pd.Series(time))
data_dict = {}
data_dict['U0'] = data.variables['U'][:,0, iLat, iLon]
data_dict['V0'] = data.variables['V'][:,0, iLat, iLon]
data_dict['U1'] = data.variables['U'][:,1, iLat, iLon]
data_dict['V1'] = data.variables['V'][:,1, iLat, iLon]
data_dict['U2'] = data.variables['U'][:,2, iLat, iLon]
data_dict['V2'] = data.variables['V'][:,2, iLat, iLon]
data_dict
data_dict.keys()
pddata = pd.DataFrame(data_dict, index=timepd)
pddata
pddata.to_csv('ExtraídasVientoRawsonMP6BL2SF2.csv')
