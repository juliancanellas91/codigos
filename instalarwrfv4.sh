#!/bin/bash -x

module load git curl cmake gcc/6.4.0

mkdir WRFV4
cd WRFV4
echo $PWD

echo "WRF"
git clone https://github.com/wrf-model/WRF

echo "WPS"
git clone https://github.com/wrf-model/WPS

echo "WRFDA"
cp -r WRF/ WRFDA

echo "WRFPLUS"
cp -r WRF/ WRFPLUS

echo "spack..."
. ../spack/share/spack/setup-env.sh

spack compiler find

spack env activate gsiULTIMAPORFAVOR

echo "variables de entorno..."
export PATH="$(spack location -i mpich)/bin:${PATH}"
export PATH="$(spack location -i netcdf-c)/bin:${PATH}"
export PATH="$(spack location -i netcdf-fortran)/bin:${PATH}"
export HDF5=$(spack location -i hdf5)
export NETCDF=$(spack location -i netcdf-fortran)
export NETCDF_C=$(spack location -i netcdf-c)
export ZLIB=$(spack location -i zlib)
export LD_LIBRARY_PATH="${ZLIB}/lib:${HDF5}/lib:${NETCDF}/lib:${LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH="$(spack location -i libpng)/lib/:$LD_LIBRARY_PATH"
export JASPER_ROOT=$(spack location -i jasper)
export JASPERLIB="${JASPER_ROOT}/lib64"
export JASPERINC="${JASPER_ROOT}/include"
export LIBPNG="$(spack location -i libpng)"
export LDFLAGS="-L$ZLIB/lib -L$JASPERLIB -L$LIBPNG/lib"
export CPPFLAGS="-I$ZLIB/include -I$JASPERINC -I$LIBPNG/include"
export FC="gfortran"
export FCFLAGS=-m64
export F77="gfortran"
export FFLAGS="-m64"
export CC="/nfs/soft/r6/gcc/6.4.0/bin/gcc"
export CXX="/nfs/soft/r6/gcc/6.4.0/bin/g++"

ulimit -s unlimited
export WRF_EM_CORE=1
export WRFIO_NCD_LARGE_FILE_SUPPORT=1

ln -sf ${NETCDF_C}/include/* ${NETCDF}/include/
ln -sf ${NETCDF_C}/lib/* ${NETCDF}/lib/
ln -sf ${NETCDF_C}/lib/pkgconfig/* ${NETCDF}/lib/pkgconfig

echo "Compilo WRF"
cd WRF

echo $PWD

./clean
./clean -a

printf '34\n1\n' | ./configure

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wrf

./compile em_real 2>&1 | tee wrf_compile.log

ls -lsh main/*.exe

cd ../WPS
echo "Compilo WPS"
echo $PWD

./clean
./clean -a

printf '1\n' | ./configure

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wps
sed -i 's|COMPRESSION_LIBS    = -L|COMPRESSION_LIBS    = '"-L${LIBPNG}/lib -L${ZLIB}/lib"' -L|' configure.wps
sed -i 's|COMPRESSION_INC     = -I|COMPRESSION_INC     = '"-I${LIBPNG}/include -L${ZLIB}/include"' -I|' configure.wps

./compile 2>&1 | tee wps_compile.log

ls -lsh ungrib/src/*.exe geogrid/src/*.exe metgrid/src/*.exe


cd ../WRFPLUS
echo "Compilo WRFPLUS"
echo $PWD

./clean
./clean -a

printf '18\n' |./configure wrfplus

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wrf

./compile wrfplus 2>&1 | tee wrfplus_compile.log

export WRFPLUS_DIR=$PWD

ls -lsh main/*.exe

#### WRFDA ####

cd ../WRFDA
echo "Compilo WRFDA"
echo $PWD

./clean
./clean -a

printf '18\n' | ./configure 4dvar

sed -i 's|LIB_EXTERNAL.*=|LIB_EXTERNAL = '"-L${ZLIB}/lib"'|' configure.wrf

./compile all_wrfvar 2>&1 | tee 4dvar_compile.log

ls -lsh var/build/*.exe var/obsproc/*.exe

