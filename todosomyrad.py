import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from minisom import MiniSom
from scipy.cluster.vq import kmeans
from scipy.stats import ks_2samp
import pandas as pd
import datetime
from pvlib import clearsky
from pvlib.location import Location
import math
from matplotlib.colors import Normalize
import matplotlib.cm as cm

path = './'
data = np.load(path+'era5_2015-2020_z850.npy')
rad_proc = pd.read_csv(path+'radiation_hourly.csv')

neuron_len = 10 # Lo recomendado
lat = 57 # Cantidad de puntos en latitud. Cambia según la variable
lon = 49 # idem
rows = neuron_len # Trabajamos con SOMs cuadrados, aunque podría cambiar.
columns = neuron_len
num_iteration = 52608 # Cantidad de pasos temporales. Cambia según el dataset.
sigma = neuron_len - 1
learning_rate = 0.05 # No tocar.
times = 0.5 # Cantidad de repeticiones de num_iteration.

# Si quiero armar un SOM con más de una variable, en este punto debería abrir
# las dos variables y hacer un
# z_trial = np.concatenate((z900,z500),axis = 1)

data = np.reshape(data,(num_iteration,lat*lon)) # SOM necesita un arrange 2-D
# con la primer dimensión representando el tiempo.
data = np.nan_to_num(data)

k_means, error = kmeans(data,neuron_len**2)
som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
for i in range(neuron_len**2):
    som._weights[i//neuron_len][i%neuron_len] = k_means[i]


som.train(data, int(num_iteration*times))
weights = som._weights

sigma = 2
learning_rate = 0.01
times = 10
som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
som._weights = weights
som.train(data, num_iteration*times)
weights = som._weights

def return_to_latlon(weights):
  pesos = np.zeros((neuron_len,neuron_len,lat,lon))
  for i in range(len(pesos)):
    for j in range(len(pesos[0])):
        k = 0
        while k < len(weights[0][0]):
          pesos[i][j][k//lon][k%lon]=weights[i][j][k]
          k += 1
  return pesos

pesos = return_to_latlon(weights)

fig, axs = plt.subplots(neuron_len, neuron_len, figsize=(15, 15))
for i in range(neuron_len):
    for j in range(neuron_len):
        axs[i][j].contour(pesos[i][j],colors='black')
        axs[i][j].imshow(pesos[i][j])
plt.show()

def DME(data,weights):
    dim = len(data[0,:])
    dme = 0
    for i in range(dim):
        result, pvalue = ks_2samp(data[:,i],weights[:,i])
        if 0.005 > pvalue:
            dme += 1
    return dme/dim

print(DME(data,weights))
print(som.quantization_error(data))
print(som.topographic_error(data))

plt.imshow(som.activation_response(data))
plt.title('activation response')
plt.colorbar()
plt.show()
#plt.close()
plt.imshow(som.distance_map(scaling='mean'))
plt.title('distance map')

plt.colorbar()



daytime_winners = np.zeros((neuron_len,neuron_len))
for instant in range(len(data)):
    if rad_proc['day'][instant]:
        daytime_winners[som.winner(data[instant])] += 1
total_winners = som.activation_response(data)
fig, axs = plt.subplots(2, 1, figsize=(15, 15))
axs[1].imshow(daytime_winners)
axs[0].imshow(total_winners)

highly_variable = rad_proc.loc[rad_proc['vi'] >5]
c = np.zeros((neuron_len,neuron_len))
for variable_instant in highly_variable.index:
    c[som.winner(data[variable_instant])] +=1

variable_as_percentage = np.zeros((neuron_len,neuron_len))
variable_as_percentage = c / daytime_winners * 100

cmap=cm.get_cmap('viridis')
#normalizer=Normalize(10,40)
#im=cm.ScalarMappable(norm=normalizer)
plt.imshow(variable_as_percentage,cmap=cmap)#,norm=normalizer)
plt.title('Horas variables como porcentaje de las horas diurnas')
plt.colorbar()#im)
plt.show()


summer = [1,2,3]
fall = [4,5,6]
winter = [7,8,9]
spring = [10,11,12]
for season in [summer,fall,winter,spring]:
    total_winners = np.zeros((neuron_len,neuron_len))
    daytime_winners = np.zeros((neuron_len,neuron_len))
    c = np.zeros((neuron_len,neuron_len))
    variable_as_percentage = np.zeros((neuron_len,neuron_len))


    for instant in range(len(data)):
        if np.isin(rad.index.month[instant],season):
            total_winners[som.winner(data[instant])] += 1
            if rad['day'][instant]:
                daytime_winners[som.winner(data[instant])] += 1
                if rad['tort'][instant] > 5:
                    c[som.winner(data[instant])] +=1
    variable_as_percentage = c / daytime_winners * 100


    fig, axs = plt.subplots(2, 2, figsize=(15, 15))
    axs1 = axs[0,0].imshow(total_winners)
    plt.colorbar(axs1, ax = axs[0,0])
    axs2 = axs[0,1].imshow(daytime_winners)
    plt.colorbar(axs2, ax = axs[0,1])
    axs3 = axs[1,0].imshow(c)
    plt.colorbar(axs3, ax = axs[1,0])
    axs4 = axs[1,1].imshow(variable_as_percentage)
    plt.colorbar(axs4, ax = axs[1,1])
    plt.savefig(str(season)+'_vi5.png')
    plt.close()
