#!/bin/bash -x

MPIRUN=/opt/intel/compilers_and_libraries_2017.0.098/linux/mpi/intel64/bin/mpirun
rm log.WRFmpirun

$MPIRUN ./geogrid.exe >> log.WRFmpirun

./ungrib.exe >> log.WRFmpirun

$MPIRUN ./metgrid.exe >> log.WRFmpirun

$MPIRUN ./real.exe >> log.WRFmpirun

start_time=$(date +%s)
$MPIRUN ./wrf.exe >> log.WRFmpirun
finish_time=$(date +%s)
echo "Time duration WRF.exe: $((finish_time - start_time)) secs." >> log.WRFtime
