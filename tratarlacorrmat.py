import numpy as np
corr_mat = np.load('correlation_matrix_sixhours_normalized.npy',allow_pickle = True)
mask_corr = np.zeros(corr_mat.shape)
for i in range(len(corr_mat)):
    for j in range(len(corr_mat[i])):
        if corr_mat[i][j] > 0.7:
            mask_corr[i][j] = 1
a = np.sum(mask_corr, axis = 0)
a.max()
np.argmax(a)
b = mask_corr[np.argmax(a)]
#second_iter = np.delete(mask_corr,np.nonzero(b),axis = 0)
#second_iter = np.delete(second_iter,np.nonzero(b),axis = 1)
archetype_list = []
archetype_list.append(np.argmax(a))
c = np.nonzero(b)[0]

second_iter = np.copy(mask_corr)

for index in c:
    second_iter[:,index] = np.zeros(len(mask_corr))
for index in c:
    second_iter[index,:] = np.zeros(len(mask_corr))




mask_corr = np.zeros(corr_mat.shape)
for i in range(len(corr_mat)):
    for j in range(len(corr_mat[i])):
        if corr_mat[i][j] > 0.7:
            mask_corr[i][j] = 1

archetype_list  = []
for i in range(100):
    a = np.sum(mask_corr, axis = 0)
    b = mask_corr[np.argmax(a)]
    archetype_list.append(np.argmax(a))
    c = np.nonzero(b)[0]
    for index in c:
        mask_corr[:,index] = np.zeros(len(mask_corr))
        mask_corr[index,:] = np.zeros(len(mask_corr))

archetype_dict  = {}
for i in range(100):
    a = np.sum(mask_corr, axis = 0)
    b = mask_corr[np.argmax(a)]
    c = np.nonzero(b)[0]
    archetype_dict[np.argmax(a)] = [c,len(c)]
    for index in c:
        mask_corr[:,index] = np.zeros(len(mask_corr))
        mask_corr[index,:] = np.zeros(len(mask_corr))


archetypes = []
for index in archetype_list:
    archetypes.append([z[index*6],clwc[index*6]])


fig, axs = plt.subplots(10, 10, figsize=(15, 15))

for i in range(10):
    for j in range(10):
        axs[i][j].imshow(archetypes[i*10+j][1], cmap='Greys')
        axs[i][j].contour(archetypes[i*10+j][0], cmap='Reds')
plt.show()


mask_corr = np.zeros(corr_mat.shape)
for i in range(len(corr_mat)):
    for j in range(len(corr_mat[i])):
        if corr_mat[i][j] > 0.6:
            mask_corr[i][j] = 1

archetype_list  = []
for i in range(100):
    a = np.sum(mask_corr, axis = 0)
    b = mask_corr[np.argmax(a)]
    c = np.nonzero(b)[0]
    archetype_list.append([np.argmax(a),len(c)])

    for index in c:
        mask_corr[:,index] = np.zeros(len(mask_corr))
        mask_corr[index,:] = np.zeros(len(mask_corr))

activation_response = np.zeros((10,10))
for i in range(len(archetype_list)):
    activation_response[i//10,i%10] = archetype_list[i][1]
plt.imshow(activation_response)
plt.colorbar()
plt.show()



mask_corr = np.zeros(corr_mat.shape)
for i in range(len(corr_mat)):
    for j in range(len(corr_mat[i])):
        if corr_mat[i][j] > 0.9:
            mask_corr[i][j] = 1

archetype_list  = []
for i in range(100):
    a = np.sum(mask_corr, axis = 0)
    b = mask_corr[np.argmax(a)]
    c = np.nonzero(b)[0]
    archetype_list.append([np.argmax(a),c,len(c)])

    for index in c:
        mask_corr[:,index] = np.zeros(len(mask_corr))
        mask_corr[index,:] = np.zeros(len(mask_corr))

intermittent_percentage = np.zeros((10,10))
for i in range(len(archetype_list)):
    intermittent = 0
    for index in archetype_list[i][1]:
        if rad_proc['vi'][index] > 5:
            intermittent += 1
    intermittent_percentage[i//10,i%10] = intermittent/archetype_list[i][2]*100
plt.imshow(intermittent_percentage)
plt.colorbar()
plt.show()

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt


from matplotlib.colors import ListedColormap

cmap = np.zeros([256, 4])
cmap[:, 3] = np.linspace(0, 1, 256)
cmap = ListedColormap(cmap)

m = Basemap(projection='cyl', llcrnrlon=-63, \
    urcrnrlon=-53.,llcrnrlat=-40,urcrnrlat=-30, \
    resolution='l')
grid_lon, grid_lat = np.meshgrid(ds['longitude'].values, ds['latitude'].values) #regularly spaced 2D grid
x, y = m(grid_lon, grid_lat)
#m.drawcoastlines(linewidth=0.25)
#m.drawmapboundary(fill_color='grey')
#m.drawcountries(linewidth=0.25)
#m.fillcontinents(color='coral',lake_color='aqua')
m.bluemarble()
m.drawparallels(np.arange(-40.,-30.,5.),labels=[1,0,0,0])
m.drawmeridians(np.arange(-60.,-55.,5.),labels=[0,0,0,1])
m.contour(x,y,ds['z'][34456,1], cmap='bwr')
plt.colorbar()
plt.show()




for i in range(neuron_len):
    for j in range(neuron_len):
        m = Basemap(projection='cyl', llcrnrlon=-63, \
        urcrnrlon=-53.,llcrnrlat=-40,urcrnrlat=-30, \
        resolution='l')
        grid_lon, grid_lat = np.meshgrid(ds['longitude'].values, ds['latitude'].values) #regularly spaced 2D grid
        x, y = m(grid_lon, grid_lat)
        m.drawcoastlines(linewidth = 0.2)
        m.drawmapboundary(linewidth = 0.2)
        m.drawcountries(linewidth = 0.2)

        #m.bluemarble()
        m.drawparallels(np.arange(-40.,-30.,5.),labels=[1,0,0,0])
        m.drawmeridians(np.arange(-60.,-55.,5.),labels=[0,0,0,1])
        m.contour(x,y,pesos_800[i][j], cmap='bwr', latlon = True)
        #m.imshow(x,y,pesos_900[i,j], cmap = cmap)
        plt.colorbar()
        plt.savefig('neuron_'+str(i)+'_'+str(j)+'.png')
        plt.close()
