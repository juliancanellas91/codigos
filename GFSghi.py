
import os
import sys
import pdb
import glob
import subprocess
import calendar
import math
import datetime
import time
import pygrib as gr
import numpy as np


idate = (2017,12,18,0,0,0)
interval = 6.0 
fdate = (2017,12,19,0,0,0)
init_data = 'gfs'
pathtoinitdata = '.' # Directorio para guardar los archivos

def download_data(date,initdata,pathtoinitdata):
    if initdata == 'gfs':
        prefix = 'gfsanl'
        n = '4'
        suffix = '.grb2'
    elif initdata == 'nam':
        prefix = 'namanl'
        n = '218'
        suffix = '.grb2'

    command = ('wget "http://nomads.ncdc.noaa.gov/data/' + prefix + '/' + date[:6] + '/' + date[:8] +
                '/' + prefix + '_' + n + '_' + date + '_003' + suffix + '" -P ' + pathtoinitdata)
    os.system(command)
    ds = gr.open(str(prefix + '_' + n + '_' + date + '_003' + suffix))
    ghi = ds()[334]
    ghi = ghi.data()[0][249,603]
    np.save(date + '_003_' + str(ghi) , ghi)
    os.remove(str(prefix + '_' + n + '_' + date + '_003' + suffix))

    command = ('wget "http://nomads.ncdc.noaa.gov/data/' + prefix + '/' + date[:6] + '/' + date[:8] +
                '/' + prefix + '_' + n + '_' + date + '_006' + suffix + '" -P ' + pathtoinitdata)
    os.system(command)
    ds = gr.open(str(prefix + '_' + n + '_' + date + '_006' + suffix))
    ghi = ds()[334]
    ghi = ghi.data()[0][249,603]
    np.save(date + '_006_' + str(ghi) , ghi)
    os.remove(str(prefix + '_' + n + '_' + date + '_006' + suffix))

    return

def get_init_files(initdata,idate,interval,fdate,pathtoinitdata):
    if initdata=='gfs':
        prefix = 'gfsanl'
    elif initdata=='nam':
        prefix = 'namanl'

    idaten = calendar.timegm(idate)
    fdaten = calendar.timegm(fdate)

    ifiledate = idaten - idaten%(interval*3600)
    ffiledate = fdaten - fdaten%(interval*3600) + (interval*3600)

    required_dates = range(int(ifiledate),int(ffiledate),int(interval*3600))

    initfiles = glob.glob(pathtoinitdata + '*')

    for r in required_dates:
        longdate = time.gmtime(r)
        fname_date = ''.join(["%02u" %x for x in longdate[0:3]]) + '_' + ''.join(["%02u" %x for x in longdate[3:5]])
        checkfiles_prefix = []
        checkfiles_date = []
        for f in initfiles:
            checkfiles_prefix.append(prefix in f)
            checkfiles_date.append(fname_date in f)
        try:
            checkfiles_prefix.index(1) + checkfiles_date.index(1)
        except ValueError:
            print ('Downloading required file for ' + fname_date)
            download_data(fname_date,init_data,pathtoinitdata)
        else:
            print ('Data for ' + fname_date + ' already exists.')
    return

get_init_files(init_data,idate,interval,fdate,pathtoinitdata)

os.system('ls | grep .npy | awk -F "." \'{print $1}\' | awk -F "_" \'{print $4}\' > GFSghi' + str(idate[0]) + str(idate[1] +1) + '.txt')
