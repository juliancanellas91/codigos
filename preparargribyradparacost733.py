import xarray as xr
import numpy as np
from pvlib import clearsky
from pvlib.location import Location

msl = np.nan_to_num(
np.reshape(
            np.fliplr( # necesario para pasar de norte -> sur a sur -> norte
                      np.array(xr.load_dataset('era5-2015-2020-mslp-horario-30406353.grib',engine = 'cfgrib')['msl'].sel(time=slice('2015-01-01','2019-12-31')))
                      ),(43824,41*41)
          ))[[i for i in range(43824) if i%3 == 0]]

np.savetxt('mslp.dat', msl, fmt='%6.3f')

import pandas as pd

dir = #local dir

def read_ceilap_rad(string):
    obs =  pd.read_csv(string, header=None, index_col='datetime',
parse_dates={'datetime': [0, 1, 2, 3, 4, 5]},
date_parser=lambda x:
pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
    obs.columns = ['sza', 'ghi']
    return obs

ds = pd.concat((read_ceilap_rad(f) for f in dir))

ds = ds.loc[ds.index.year < 2023]

era = era.sel(time=era.time.dt.year.isin([2017,2018,2019,2020,2021,2022]))


window = 30  # Number of values taken in the rolling operations

loc = Location(-34.603722, -58.381592) # location: Buenos Aires
ds.index = ds.index.tz_localize('America/Argentina/Buenos_Aires')
cs = loc.get_clearsky(ds.index.tz_convert('America/Argentina/Buenos_Aires'),
                      model='ineichen', linke_turbidity=3)  #

def tortuosity(values):
    return np.abs(np.diff(values)).sum()

def arc_length(x):
    npts = len(x)
    arc = 0
    for k in range(1, npts):
        arc += np.sqrt((x[k] - x[k-1])**2 + 1)
    return arc


"""
Necesito el argumento "step" que tiene rolling en pandas 2.1.4+
porque no puedo usarlo!!
"""
ds['cs'] = cs['ghi']

# ds['kt'] = ds['ghi']/ds['cs']

# ds['tort'] = ds['ghi'].rolling(window, step=60).aggregate(arc_length)
#
# ds['cs_tort'] = ds['cs'].rolling(window, step=60).aggregate(arc_length)
#
# ds['vi'] = ds['tort']/ds['cs_tort']
#
#
# ds['kt'] = rad['ghi'].rolling(window=datetime.timedelta(hours=1), step=None).mean()/rad['cs'].rolling(window=datetime.timedelta(hours=1), step=None).mean()

rad =



axes = week.plot(subplots=True, sharex=True, legend=True)
axes[0].fill_between(week.index, 0, max(week['ghi']), where=week['tort'] > 50,
                     alpha=0.5)
plt.show()


def compute_vi(df, freq='H'):
    df1 = pd.DataFrame()

    df1['dghi2'] = (df['ghi'].diff().shift(periods=-1))**2
    df1['dcs2'] = (df['cs'].diff().shift(periods=-1))**2
    df1 = df1[:-1]
    df1['dt2'] = ((df.index[1:] - df.index[:-1]).total_seconds() / 60).astype('float') ** 2

    df1['ghi_al'] = (df1['dghi2'] + df1['dt2']) ** 0.5
    df1['cs_al'] = (df1['dcs2'] + df1['dt2']) ** 0.5

    return df1['ghi_al'].resample(freq).sum() / df1['cs_al'].resample(freq).sum()

def compute_kt(df, freq='H'):
    return df['ghi'].groupby(pd.Grouper(freq=freq)).sum() / df['cs'].groupby(pd.Grouper(freq=freq)).sum()
