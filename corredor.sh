#!bin/bash -x

cd "/home/julian/Documentos/Proyectos/SDEWES_2020/Corridas"

for years in "/home/julian/Documentos/Proyectos/SDEWES_2020/Corridas/"*/; do
	for months in "$years"*/; do
		cd "$months"
		for sims in "$months"*/; do
			cd "$sims"
	                python Paraextraerunpardeseries.py
		done
	done
done
