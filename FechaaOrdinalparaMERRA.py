#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 15:50:57 2019

@author: julian
"""

import datetime

#####Editar estos datos a piaccere#####

#Los datos de MERRA van de 01 01 1980 - 30 06 2019

AÑO_INICIO = 1980

MES_INICIO = 1 

DIA_INICIO = 1

AÑO_FINAL = 2019

MES_FINAL = 6

DIA_FINAL = 30

######################################

#Siempre empiezo en el primer dato de un día y termino en el último. 

inicio = datetime.datetime(AÑO_INICIO,MES_INICIO,DIA_INICIO, 00,30) # Las horas y minutos acá no aportan nada

final = datetime.datetime(AÑO_FINAL,MES_FINAL,DIA_FINAL, 23, 30)    # idem

#La función datetime.toordinal me devuelve el dato que quiero pero parece no 
#estar de acuerdo con la convención adoptada en MERRA. 
#Yo utilicé datetime para convertir 01 01 1980 a ordinal y me da 722815. 
#MERRA asigna el valor 722816 a ese dato
