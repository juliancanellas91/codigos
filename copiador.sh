#!/bin/bash -x

cd /nfs/home/jcanellas
for years in /nfs/home/jcanellas/SDEWES\ 2020/* ; do
	cd "$years"
	for months in */ ; do
		echo "$months"
		echo "copiando DATA"
		cp -r ../../DATA $months/
                echo "copiando WPS"
                cp -r ../../WPS $months/
                echo "copiando WRF"
                cp -r ../../WRF $months/
	done
done


