import pandas as pd
import numpy as np
import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

plt.rcParams["figure.figsize"] = (20,10)

from pvlib import clearsky
from pvlib.location import Location

def read_ceilap_radiation(filelst, tzinfo=None):
    if type(filelst) is not list:
        filelst = [filelst]

    rad = pd.DataFrame()
    for file in filelst:
        # aux = pd.read_csv(file)  # For csv files
        # aux = pd.read_excel(file)  # For xls or xlsx files
        aux = pd.read_csv(file, skiprows=0, header=None, index_col='datetime',
                          parse_dates={'datetime': [0, 1, 2, 3, 4, 5]},
                          date_parser=lambda x:
                          pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
        aux.columns = ['sza', 'ghi']

        rad = pd.concat([rad, aux])
    rad = rad.sort_index().tz_localize(tzinfo)
    return rad

def arc_length(x, y):
    npts = len(x)
    arc = 0
    for k in range(1, npts):
        arc += np.sqrt((x[k] - x[k-1])**2 + (y[k] - y[k-1])**2)
    return arc

data_2018 = pd.read_csv('data_2018.csv', header = None)

def tortuosity(values):
    return np.abs(np.diff(values)).sum()

since = '2017-12-19'
delta = '7 day'
window = 30  # Number of values taken in the rolling operations

loc = Location(-34.603722, -58.381592) # location: Buenos Aires

file = 'data_2017_cielap.csv'  # File with input data


rad = pd.read_csv(file, names=['year', 'month', 'day', 'hour', 'minute', 'second', 'nose', 'radiation'])
# rad.index = pd.to_datetime(rad[['year', 'month', 'day', 'hour', 'minute', 'second']])

radiation = rad['radiation']
radiation.index = pd.to_datetime(rad[['year', 'month', 'day', 'hour', 'minute', 'second']])

since = pd.to_datetime(since)
to = since + pd.Timedelta(delta)

rad_proc = pd.DataFrame()
rad_proc['radiation'] = radiation[since:to]
rad_proc.index = rad_proc.index.tz_localize('America/Argentina/Buenos_Aires')

rad_proc['mean'] = rad_proc['radiation'].rolling(window, center=True).mean()

cs = loc.get_clearsky(rad_proc.index.tz_convert('America/Argentina/Buenos_Aires'),
                      model='ineichen', linke_turbidity=3)  # Clear-sky radiation
rad_proc['cs'] = cs['ghi']

rad_proc['std'] = rad_proc['radiation'].rolling(window, center=True).std()
rad_proc['tort'] = rad_proc['radiation'].rolling(window, center=True).aggregate(tortuosity)

# Normalize with the clean-sky value
rad_proc['norm_std'] = rad_proc['std'] / rad_proc['cs']
rad_proc['norm_tort'] = rad_proc['tort'] / rad_proc['cs']

rad_proc['norm_std'][rad_proc['cs'] < 100] = np.nan
rad_proc['norm_tort'][rad_proc['cs'] < 100] = np.nan


threshold = 1.5

axes = rad_proc.plot(subplots=True, sharex=True, legend=True)
# axes = rad_proc.plot(ylim=[0, 1500])

print(axes)
axes[0].fill_between(rad_proc.index, 0, max(rad_proc['radiation']), where=rad_proc['norm_tort'] > threshold,
                     alpha=0.5)
# axes[5].fill_between(rad_proc.index, 0, max(rad_proc['norm_tort']), where=rad_proc['norm_tort'] > threshold,
#                      alpha=0.5)
plt.show()

