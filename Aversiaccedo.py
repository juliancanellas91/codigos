#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 13:35:33 2019

@author: julian
"""

import xarray
from pydap.client import open_url
from pydap.cas.urs import setup_session
username= 'juliancanellas'
password= '49242964Gollo'
url='https://goldsmr4.gesdisc.eosdis.nasa.gov/dods/M2T1NXAER'
# url='https://goldsmr4.gesdisc.eosdis.nasa.gov/dods/M2T1NXSLV'

session = setup_session(username=username,password=password,check_url=url)
pydap_ds = open_url(url, session=session)
store = xarray.backends.PydapDataStore(pydap_ds)
ds = xarray.open_dataset(store)
