import numpy as np
import xarray as xr
import netCDF4 as nc
import matplotlib.pyplot as plt
from minisom import MiniSom
from scipy.cluster.vq import kmeans


"""
Selecciono la variable de reanálisis a trabajar
"""

dataset = # Colocar aquí el nombre del dataset
ds = xr.load_dataset(dataset, engine ='cfgrib')

variable = ds['variable'][:,1,:,:] # Cambiar variable por el nombre de la
#variable deseada. Es necesario extraer el arrange de numpy para trabajar con
#SOM. La forma del arrange cambia según la variable.

"""
Preparo el SOM
"""

neuron_len = 5 # Lo recomendado
lat = 41 # Cantidad de puntos en latitud. Cambia según la variable
lon = 61 # idem
rows = neuron_len # Trabajamos con SOMs cuadrados, aunque podría cambiar.
columns = neuron_len
num_iteration = 14608 # Cantidad de pasos temporales. Cambia según el dataset.
sigma = neuron_len - 1
learning_rate = 0.005 # No tocar.
times = 1 # Cantidad de repeticiones de num_iteration.

# Si quiero armar un SOM con más de una variable, en este punto debería abrir
# las dos variables y hacer un
# z_trial = np.concatenate((z900,z500),axis = 1)

data = np.array(z)
data = np.reshape(data,(num_iteration,lat*lon)) # SOM necesita un arrange 2-D
# con la primer dimensión representando el tiempo.
data = np.nan_to_num(data)


"""
Inicializo el SOM con k-means
"""

k_means, error = kmeans(data,neuron_len**2)
som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
for i in range(neuron_len**2):
    som._weights[i//neuron_len][i%neuron_len] = k_means[i]

"""
Realizo un primer entrenamiento con parámetros 'gruesos'
"""

som.train(data, num_iteration*times)
weights = som._weights

"""
Repito el entrenamiento con parámetros 'finos'
"""

sigma = 2
learning_rate = 0.001
times = 10
som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
som._weights = weights
som.train(data, num_iteration*times)
