#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 16:12:30 2019

@author: shaper
"""
import pandas as pd


obs2017 = pd.read_csv('/home/julian/Documentos/Proyectos/SDEWES 2020/Datos Observados/CITEDEF/data_2015.csv', skiprows=31, header=None, index_col='datetime',
parse_dates={'datetime': [0, 1, 2, 3, 4, 5]},
date_parser=lambda x:
pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
obs2017.columns = ['sza', 'ghi']
#filtro 10minutales
obs2017 = obs2017.resample('10T').asfreq()
#filtro diurnos
obs2017 = obs2017.between_time('05:30','20:30')

ghi = obs2017['ghi']

date = ghi.index.date

import datetime
i = 0
while date[i] != datetime.date(2017,11,30):
    i += 1
print(i)

ghidiciembre = ghi[i:len(ghi)]

ghidiciembre = ghidiciembre.tz_localize('America/Argentina/Buenos_Aires').tz_convert('UTC')

#ghidiciembre

ceilap1217r1 = pd.read_csv('ExtraídasRadiación.csv', skiprows=31, header=None, index_col='datetime',
parse_dates={'datetime': [0]},
date_parser=lambda x:
pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
ceilap1217r1.columns = ['ghi', 'dni', 'dif', 'T2', 'U10', 'V10']

#ceilap1217r1 = ceilap1217r1.resample('10T').asfreq()
ceilap1217r1 = ceilap1217r1.between_time('08:30','23:30')

#ghidiciembre = ghidiciembre.tz_localize('America/Argentina/Buenos_Aires').tz_convert('UTC')

#ceilap1217r1['ghi'][2500:2800].plot()
#ghidiciembre[2500:2800].plot()

len(ghidiciembre) == len(ceilap1217r1)

dif = []
for i in range(len(ghidiciembre)):
    dif.append(ceilap1217r1['ghi'][i] - ghidiciembre[i])

for i in range(len(dif)):
    if ~(abs(dif[i])>0.00001):
        dif[i] = 0
        
dif2 = []
for i in range(len(dif)):
    dif2.append(dif[i]**2)    

import os
import numpy as np

os.system( 'touch ' + str(int(np.round(np.sqrt(np.mean(dif2))))) + '.txt')
