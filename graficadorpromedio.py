import this
import pandas as pd
from matplotlib import pyplot as plt
from glob import glob
import numpy as np



meses = ['marzo', 'Junio', 'Septiembre', 'Diciembre']
Anos = ['2015', '2017']
for ano in Anos:
    rmse = []
    max = []
    min = []
    for mes in meses:
        del_mes = glob('./*'+ ano + '_' + mes + '*')
        del_mes.sort()

        rmse_mensual = []
        max_mensual = []
        min_mensual = []
        gfs = pd.read_csv(del_mes[1],header= None, index_col=0, parse_dates=[0],
                                date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        gfs = gfs.tz_localize('UTC').tz_convert('UTC')
        gfs = gfs[gfs[1] != 0]
        obs = pd.read_csv(del_mes[0],header= None, index_col=0, parse_dates=[0],
                                date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
        obs = obs.tz_localize('America/Argentina/Buenos_Aires').tz_convert('UTC')
        obs = obs[obs[1] != 0]
        obs = obs.resample('3H',loffset='1.5H').mean()
        for sims in del_mes[2:]:
            sim = pd.read_csv(sims, index_col=0,
                                parse_dates=[0], date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S+00:00'))
            sim = sim.tz_localize('UTC').tz_convert('UTC')
            sim = sim[sim['Mean'] != 0]
            sim = sim.resample('3H',loffset='1.5H').mean()
            monthly_rmse = ((sim['Mean']-obs[1])**2).resample('1D').mean()**.5
            rmse_mensual.append(monthly_rmse.mean())
            max_mensual.append(monthly_rmse.max())
            min_mensual.append(monthly_rmse.min())
        monthly_rmse = ((gfs[1]-obs[1])**2).resample('1D').mean()**.5
        rmse_mensual.append(monthly_rmse.mean())
        max_mensual.append(monthly_rmse.max())
        min_mensual.append(monthly_rmse.min())
        rmse.append(rmse_mensual)
        max.append(max_mensual)
        min.append(min_mensual)


    n_groups = len(meses)
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.15
    opacity = 0.8

    simus = []
    for i in range(2,len(del_mes)):
        simus.append(del_mes[i].rsplit('/')[1].rsplit('_')[0]+del_mes[i].rsplit('/')[1].rsplit('_')[4].rsplit('.')[0])
    simus.append(del_mes[1].rsplit('/')[1].rsplit('_')[0])

    per_sim = []
    for i in range(len(rmse[0])):
        per_sim.append([])
        for j in range(len(meses)):
            per_sim[i].append(rmse[j][i])
    per_sim_max = []
    for i in range(len(max[0])):
        per_sim_max.append([])
        for j in range(len(meses)):
            per_sim_max[i].append(max[j][i])
    per_sim_min = []
    for i in range(len(min[0])):
        per_sim_min.append([])
        for j in range(len(meses)):
            per_sim_min[i].append(min[j][i])

    for i in range(len(per_sim)):
        bar_mensual = plt.bar(index + i*bar_width, per_sim[i], bar_width, alpha=opacity, label=simus[i])
        bar_extreme = plt.bar(index+ i*bar_width, per_sim_max[i],0.02 , bottom= per_sim_min[i], alpha=opacity, color='k')
    months = ['march','june','september','december']
    plt.xlabel('Month')
    plt.ylabel('RMSE (W/m²)')
    plt.title('Monthly mean of daily root mean squared errors for 3-hour series for year ' + ano)
    plt.xticks(index, months)
    plt.legend(loc=(1,0))
    plt.tight_layout()
    plt.savefig('RMSE'+ano+'SERIEpromediadatrihoraria.png')
    plt.show()
