#!/bin/bash

# Hola, este script convierte ficheros NETCDF al formato que lee WPS (bil), para geogrid.
# También creamos el index.

entrada=$1  #g4.timeAvgMap.MOD08_D3_6_AOD_550_Dark_Target_Deep_Blue_Combined_Mean.20170607-20170607.180W_90S_180E_90N.nc
salida=$2 #out.bil
scale10=$3 #debe ser negativo #1E-6
description=$4 #"AOD at 550nm"
units=$5
nombre=$6

min=0
min2=0

scalepositive=${scale10#\-}
max=1
max2=1E$scalepositive


echo "gdal_translate -ot Int32 -of ENVI -scale $min $max $min2 $max2 $entrada $nombre/$salida"
gdal_translate -ot Int32 -of ENVI -scale $min $max $min2 $max2 $entrada $nombre/$salida

echo "

type=continuous
projection=regular_ll         #<---------DUDA: polar_wgs84 ? creo que no
dx=1
dy=1
known_x=1.0
known_y=1.0 #2351.0           # <- edit
known_lat = -90 #40.096571     # <- edit
known_lon = -180 #-105.405615 # <- edit
#truelat1=29.5
#truelat2=45.5
#stdlon=-96.0
wordsize=4                    # <----------DUDA
scale_factor=1E$scale10       # <----------DUDA: creo que ya bien
row_order=bottom_top
signed=yes
tile_x=360                    # <- edit
tile_y=180                    # <- edit
tile_z=1
units=$units  #'unitless'     # <----------DUDA: creo que ya bien
description=$description #'AOD at 550nm'

" > $nombre/index
