'''
Module with functions to read Argentinean SMN radiation data
'''

import pandas as pd
import datetime


def read_smn_radiation(filelst, tzinfo=None):
    if type(filelst) is not list:
        filelst = [filelst]

    smn_rad = pd.DataFrame()
    for file in filelst:
        # aux = pd.read_csv(file)  # For csv files
        aux = pd.read_excel(file)  # For xls or xlsx files

        # FIXME: SMN file headings are not consistent among them, make this
        # fuction robust against that
        variables = {'IRRADIANCIA (W/m2)': 'ghi',
                     'DIF CORREGIDA (W/m2)': 'dhi'}
        anio = [str(x) for x in aux['AÑO'].tolist()]
        dia = aux['DIA JULIANO'].apply(lambda x: '{0:03d}'.format(x)).tolist()
        hrmin = aux['HORA'].apply(lambda x: '{0:04d}'.format(x))
        hora = [x[0:2] for x in hrmin]
        minuto = [x[2:4] for x in hrmin]

        # Correct 24:00 -> 00:00 the following day
        dia2 = list(map(lambda x, y: '{0:03d}'.format(int(x) + int(int(y)/24)),
                        dia, hora))
        hora2 = list(map(lambda x: '{0:02d}'.format(int(x) % 24), hora))
        # Correct last date of the year to the first of the following year
        if int(dia2[-1]) > 365:
            anio[-1] = str(int(anio[-1]) + 1)
            dia2[-1] = '{0:03d}'.format(1)

        aux = aux[list(variables.keys())]
        aux = aux.rename(columns=variables)

        strzip = zip(anio, dia2, hora2, minuto)
        concatstr = ["".join(x) for x in strzip]
        datetimlst = [datetime.datetime.strptime(i, "%Y%j%H%M")
                      for i in concatstr]
        index = pd.DatetimeIndex(datetimlst)
        aux = aux.set_index(index)

        smn_rad = pd.concat([smn_rad, aux])
    smn_rad = smn_rad.sort_index().tz_localize(tzinfo)
    return smn_rad


def read_smn_radiation2(filelst, tzinfo=None):
    if type(filelst) is not list:
        filelst = [filelst]

    smn_rad = pd.DataFrame()
    for file in filelst:
        # aux = pd.read_csv(file)  # For csv files
        aux = pd.read_excel(file)  # For xls or xlsx files

        # FIXME: SMN file headings are not consistent among them, make this
        # fuction robust against that
        variables = {'IRRADIANCIA (W/m2)': 'ghi',
                     'DIF CORREGIDA (W/m2)': 'dhi'}

        datezip = zip(aux['FECHA'], aux['HORA'], aux['MINUTO'])
        datetimlst = [datetime.datetime(x[0].year, x[0].month, x[0].day,
                      int(round(x[1])), int(round(x[2]))) for x in datezip]

        aux = aux[list(variables.keys())]
        aux = aux.rename(columns=variables)

        index = pd.DatetimeIndex(datetimlst)
        aux = aux.set_index(index)

        smn_rad = pd.concat([smn_rad, aux])
    smn_rad = smn_rad.sort_index().tz_localize(tzinfo)
    return smn_rad


def dni_from_smn_data():
    pass


def read_ceilap_radiation(filelst, tzinfo=None):
    if type(filelst) is not list:
        filelst = [filelst]

    rad = pd.DataFrame()
    for file in filelst:
        # aux = pd.read_csv(file)  # For csv files
        # aux = pd.read_excel(file)  # For xls or xlsx files
        aux = pd.read_csv(file, skiprows=31, header=None, index_col='datetime',
                          parse_dates={'datetime': [0, 1, 2, 3, 4, 5]},
                          date_parser=lambda x:
                          pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
        aux.columns = ['sza', 'ghi', 'uva', 'uvb', 'iuv']

        rad = pd.concat([rad, aux])
    rad = rad.sort_index().tz_localize(tzinfo)
    return rad
