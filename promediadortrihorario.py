#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 18:14:22 2019

@author: julian
"""

import pandas as pd


obs2017 = pd.read_csv('/home/julian/Documentos/Proyectos/SDEWES 2020/Datos Observados/CITEDEF/data_2015.csv', skiprows=31, header=None, index_col='datetime',
parse_dates={'datetime': [0, 1, 2, 3, 4, 5]},
date_parser=lambda x:
pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
obs2017.columns = ['sza', 'ghi']


ghi = obs2017['ghi']

date = ghi.index.date

import datetime
i = 0
while date[i] != datetime.date(2015,2,28):
    i += 1
print(i)
j = 0
while date[j] != datetime.date(2015,4,1):
    j += 1
print(j)
ghidiciembre = ghi[i:j]

for i in range(len(ghidiciembre)):
    if ghidiciembre[i] > 1250:
        ghidiciembre[i] = ghidiciembre[i]/1000
ghidiciembre = ghidiciembre.tz_localize('America/Argentina/Buenos_Aires').tz_convert('UTC')

ceilap1217r1 = pd.read_csv('ExtraídasRadiación.csv', skiprows=31, header=None, index_col='datetime',
parse_dates={'datetime': [0]},
date_parser=lambda x:
pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
ceilap1217r1.columns = ['ghi', 'dni', 'dif', 'T2', 'U10', 'V10']

ceilap1217r1 = ceilap1217r1.tz_localize('UTC').tz_convert('UTC')




promediotrihorario = [[],[]]
init_hour = [0,3,6,9,12,15,18,21]
end_hour = [3,6,9,12,15,18,21,0]

i = 114
j = 114
for days in range(22):
    for hours in range(len(init_hour)):
        while i < len(ceilap1217r1.index) and ceilap1217r1.index[i].hour < init_hour[hours]:
            i += 1
        #print( 'i=', i ,ceilap1217r1.index[i])
        while j < len(ceilap1217r1.index) and  ceilap1217r1.index[j].hour < end_hour[hours]:
            j += 1
        #print('j=', j,ceilap1217r1.index[j])
        promediotrihorario[0].append(ceilap1217r1.index[i])
        promediotrihorario[1].append(ceilap1217r1['ghi'][i:j].mean())
        if (i < len(ceilap1217r1.index)) and ceilap1217r1.index[i].hour == 21:
            i += 18
        if (j < len(ceilap1217r1.index)) and ceilap1217r1.index[j].hour == 21:
            j += 18
                
promediosimulado = promediotrihorario

promediotrihorario = [[],[]]
init_hour = [0,3,6,9,12,15,18,21]
end_hour = [3,6,9,12,15,18,21,0]

i = 1260
j = 1260
for days in range(31):
    for hours in range(len(init_hour)):
        while i < len(ghidiciembre.index) and ghidiciembre.index[i].hour < init_hour[hours]:
            i += 1
        #print( 'i=', i ,ceilap1217r1.index[i])
        while j < len(ghidiciembre.index) and  ghidiciembre.index[j].hour < end_hour[hours]:
            j += 1
        #print('j=', j,ceilap1217r1.index[j])
        promediotrihorario[0].append(ghidiciembre.index[i])
        promediotrihorario[1].append(ghidiciembre[i:j].mean())
        if (i < len(ghidiciembre.index)) and ghidiciembre.index[i].hour == 21:
            i += 180
        if (j < len(ghidiciembre.index)) and ghidiciembre.index[j].hour == 21:
            j += 180
                
promedioobservado = promediotrihorario


dif = []
j = 0
for i in range(len(promediosimulado[0])):
    while promediosimulado[0][i] != promedioobservado[0][j]:
        j += 1
    dif.append(promediosimulado[1][i]-promedioobservado[1][j])


