import this
import ncepbufr

bufr = ncepbufr.open('gdas.1bamua.t00z.20171219.bufr')
datosutiles = open('aquiestatodo.txt','w+')

while bufr.advance() == 0:
     while bufr.load_subset() == 0:
         # bufr.print_subset()
         lon = bufr.read_subset('CLON')[0][0]
         lat = bufr.read_subset('CLAT')[0][0]
         if lon > -80 and lon < -45 and lat > -45 and lat < -20:
             lon = round(lon,2)
             lat = round(lat,2)
             date = bufr.msg_date
             TBr = round(bufr.read_subset('TMBR')[0][0])
             datosutiles.write(str(lat)+' '+str(lon)+' '+str(date)+' '+str(TBr)+' '+'\n')
datosutiles.close()
