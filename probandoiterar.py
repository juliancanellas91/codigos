import shutil
import time
pathtoWRF = '/home/julian/Documentos/WRFARW/Build_WRF/WRF/test/em_real/'

def edit_namelist_input(old,new,pathtoWRF=pathtoWRF):
    ninput = open(pathtoWRF+'namelist.input','r').readlines()
    for idx, line in enumerate(ninput):
        if old in line:
            # Prefix for soil intermediate data filename
            ninput[idx]= ninput[idx][:39] + new + " \n"
            nameout = open(pathtoWRF+'namelist.input','w')
            nameout.writelines(ninput)
            nameout.close()
            break
    return

for scheme in range(1,5):
    mp_physics = (scheme,scheme,scheme) # este lo agregué yo y voy a querer iterarlo.
    edit_namelist_input("mp_physics",', '.join([str(m) for m in mp_physics])+',')
    shutil.copy('/home/julian/Documentos/WRFARW/Build_WRF/WRF/test/em_real/namelist.input', '/home/julian/Documentos/Rutinas/namelist'+str(scheme)+'.input')
    print(scheme)
