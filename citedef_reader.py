import pandas as pd
from glob import glob
import datetime

def read_ceilap_radiation(filelst, tzinfo=None):
    if type(filelst) is not list:
        filelst = [filelst]

    rad = pd.DataFrame()
    for file in filelst:
        # aux = pd.read_csv(file)  # For csv files
        # aux = pd.read_excel(file)  # For xls or xlsx files
        aux = pd.read_csv(file, skiprows=31, header=None, index_col='datetime',
                          parse_dates={'datetime': [0, 1, 2, 3, 4, 5]},
                          date_parser=lambda x:
                          pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
        aux.columns = ['sza', 'ghi']

        rad = pd.concat([rad, aux])
    rad = rad.sort_index().tz_localize(tzinfo)
    return rad

def recorta_mes(pdseries,year_ini, month_ini, day_ini, year_end, month_end, day_end):
    i = 0
    while date[i] != datetime.date(year_ini,month_ini,day_ini):
        i += 1
    j = 0
    while date[j] != datetime.date(year_end,month_end,day_end):
        j += 1
    recortada = pdseries[i:j]
    return recortada

filelist = glob('/home/julian/Documentos/Proyectos/SDEWES_2020/Datos_Observados/CITEDEF/y*.dat')

for files in filelist:
    ds = read_ceilap_radiation(files)


    ### TENGO QUE CAMBIAR ESTO
    # for i in range(len(ds['ghi'])):
    #     if round(ds['ghi'][i]) == ds['ghi'][i]:
    #         ds['ghi'][i] = ds['ghi'][i]/1000





    ghi = ds['ghi']
    ghi = ghi.loc[lambda ghi: ghi < 1300]
    i = 0
    while (ghi.index.hour[i] != 1) or (ghi.index.minute[i] != 30):
        i += 1
    j = len(ghi)-1
    while (ghi.index.hour[j] != 22) or (ghi.index.minute[j] != 30):
        j -=1
    ghi = ghi[i:j]

    ghi = ghi.resample('3H',loffset='1.5H').mean()
    ghi = ghi.tz_localize('America/Argentina/Buenos_Aires').tz_convert('UTC')
    ghi.to_csv(str(files).rsplit('.')[0] + 'trihorario.csv')


filelist = glob('/home/julian/Documentos/Proyectos/SDEWES_2020/Datos_Observados/CITEDEF/*trihorario.csv')

for files in filelist:
    ds = pd.read_csv(files, header=None, index_col=0, parse_dates=[0],
                        date_parser=lambda x: pd.datetime.strptime(x, '%Y-%m-%d %H:%M:%S+00:00'))
    ghi = ds
    for months in [3,6,9,12]:
        i , j = 0 , 0
        while (ghi.index.month[i] != months):
            i += 1
        while (ghi.index.month[i+j] == months):
            j +=1
        if months == 3:
            mes = 'marzo'
        elif months == 6:
            mes = 'Junio'
        elif months == 9:
            mes = 'Septiembre'
        elif months == 12:
            mes = 'Diciembre'
        ghi[i:i+j].to_csv('CITEDEFcorregido_' + str(files).rsplit('/')[8].rsplit('y')[1].rsplit('.')[0].rsplit('t')[0] + '_' + mes + '_trihorario.csv')
