#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 15:50:57 2019

@author: julian
"""

import datetime

#####Editar estos datos a piaccere#####

#Los datos de MERRA van de 01 01 1980 - 30 06 2019

AÑO_INICIO = 2017 

MES_INICIO = 1 

DIA_INICIO = 1

AÑO_FINAL = 2018

MES_FINAL = 1

DIA_FINAL = 1

######################################

#Siempre empiezo en el primer dato de un día y termino en el último. 

inicio = datetime.datetime(AÑO_INICIO,MES_INICIO,DIA_INICIO, 00,30)

final = datetime.datetime(AÑO_FINAL,MES_FINAL,DIA_FINAL, 23, 30)    

