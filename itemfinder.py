import this
from glob import glob
import pygrib as gr
keylist = [0]
for years in glob('/home/julian/Documentos/Proyectos/SDEWES 2020/GFSghi/*/'):
    print(str(years).rsplit('/')[7])
    for months in glob(years + '*/'):
        print(str(months).rsplit('/')[8])
        filelist = glob(months + 'gfsanl*.grb2')
        for files in filelist:
#            print(str(files).rsplit('_')[2] + ' ' + str(files).rsplit('_')[4].rsplit('.')[0])
            ds = gr.open(files)
            a = str(ds()).rsplit(',')
            for line in a:
                if "Downward short-wave radiation flux" in line:
                    if line[1:4] != keylist[len(keylist)-1]:
                        print(line[1:4])
                        keylist.append(line[1:4])
