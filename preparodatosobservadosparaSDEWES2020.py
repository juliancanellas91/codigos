#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 16:50:07 2019

@author: julian
"""

######################### TRABAJO CON DATOS CEILAP ###############################

import pandas as pd

ds2015 = pd.read_csv('data_2015.csv',skiprows=31, header=None, index_col='datetime',
parse_dates={'datetime': [0, 1, 2, 3, 4, 5]}, date_parser=lambda x: 
pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
ds2015.columns = ['sza', 'ghi']

ds2017 = pd.read_csv('data_2017.csv',skiprows=31, header=None, index_col='datetime',
parse_dates={'datetime': [0, 1, 2, 3, 4, 5]}, date_parser=lambda x: 
pd.datetime.strptime(x, '%Y %m %d %H %M %S'))
ds2017.columns = ['sza', 'ghi']

ghi2015 = ds2015['ghi']
ghi2017 = ds2017['ghi']
    
import numpy as np

radlist2015 = []
for i in range(len(ghi2015)):
    if ghi2015[i] != ghi2015[0] and not np.isnan(ghi2015[i]):
        if ghi2015[i]>1300:
            ghi2015[i] = ghi2015[i]/1000
        radlist2015.append(ghi2015[i])
        
radlist2017 = []
for i in range(len(ghi2017)):
    if ghi2017[i] != ghi2017[0] and not np.isnan(ghi2017[i]):
        if ghi2017[i]>1300:
            ghi2017[i] = ghi2017[i]/1000
        radlist2017.append(ghi2017[i])
        
import matplotlib.pyplot as plt


plt.plot(radlist2015)
plt.show()
plt.plot(radlist2017)
plt.show()


asd = pd.timedelta_range('05:30:00','20:30:00', 91)
# Esto me da una matriz con instantes de tiempo 10minutales entre las 6 y las 21
asd = asd.to_native_types()

rad10min2015 = []
for time in asd:
    rad10min2015.append(ghi2015.at_time(time))


# Con esto puedo bajar de 1 min a 10 min. No promedia ni nada raro.
ghi2015 = ghi2015.resample('10T').asfreq()
ghi2017 = ghi2017.resample('10T').asfreq()


# con esto puedo filtrar sin pasar a lista, pero estoy creando huecos en la serie, o sea esto me serviria para calcular errores y graficar capaz

ghi2015 = ghi2015.loc[ ghi2015>1]
ghi2015 = ghi2015.loc[ghi2015 < 1200]

# ahora me faltaria separar por meses


########################################################################################
