 . ../../spack/share/spack/setup-env.sh
 scl-devtoolset-6
 spack compiler find
 spack env activate wrf-scl6
 export PATH="$(spack location -i mpich)/bin:${PATH}"
 export PATH="$(spack location -i netcdf)/bin:${PATH}"
 export PATH="$(spack location -i netcdf-fortran)/bin:${PATH}"
 # Environment variables required by WRF (most original naming ever)
 export HDF5=$(spack location -i hdf5)
 export NETCDF=$(spack location -i netcdf-fortran)
 export ZLIB=$(spack location -i zlib)
 # run-time linking
 export LD_LIBRARY_PATH="${ZLIB}/lib:${HDF5}/lib:${NETCDF}/lib:${LD_LIBRARY_PATH}"
 # this prevents segmentation fault when running the model
 ulimit -s unlimited
 export WRF_EM_CORE=1
 export WRFIO_NCD_LARGE_FILE_SUPPORT=1 
 spack location -i netcdf && NETCDF_C=$(spack location -i netcdf)
 ln -sf "${NETCDF_C}/include/*" "${NETCDF}/include/"
 ln -sf "${NETCDF_C}/lib/*" "${NETCDF}/lib/"
 export JASPER_ROOT=$(spack location -i jasper@2.0.14)
 export JASPERLIB="${JASPER_ROOT}/lib64"
 export JASPERINC="${JASPER_ROOT}/include"
 
