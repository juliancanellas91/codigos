#!/bin/sh
#---------------------------------------------
# SLURM script to queue parallel WRF in TUPAC
#---------------------------------------------

nodes=2
cores=32
partition=fast
time=15:00

echo "Queueing parallel job" $(pwd)
echo "at : " $(date)
echo "using " $(($nodes*$cores)) " processors in " $partition

sbatch -n $(($nodes*$cores)) -p $partition --time=$time --tasks-per-node=$cores ./WRF.sh

