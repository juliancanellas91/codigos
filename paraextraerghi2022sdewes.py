import datetime
import pandas as pd
import numpy as np
import netCDF4 as nc
from glob import glob

sims = ['6111444',
 '6111554',
 '622444',
 '622554',
 'SOLAR',
 'SOLAR_2']

months = ['out-2015-03',
 'out-2015-06',
 'out-2015-09',
 'out-2015-12',
 'out-2017-03',
 'out-2017-06',
 'out-2017-09',
 'out-2017-12']

iLat = 10
iLon = 20

sim_dict = {}
for sim in sims:
    current_sim = pd.DataFrame()
    for month in months:
        lust = glob('./'+sim+'/'+month+'/wrfout_d01_2*_0016')
        current_month = pd.DataFrame()
        for i in range(len(lust)):
            vm = nc.Dataset(lust[i])
            time = []
            for t in vm.variables['Times'][:]:
                a = t.tostring().decode().split('_')
                c = a[0].split('-') + a[1].split(':')
                time.append(datetime.datetime(*list(map(int, c))))
            timepd = pd.DatetimeIndex(pd.Series(time))
            data_dict = {}
            data_dict['ghi'] = vm.variables['SWDOWN'][:, iLat, iLon]
            data_dict
            data_dict.keys()
            pddata = pd.DataFrame(data_dict, index=timepd)
            pddata
            current_month = current_month.append(pddata)
        current_sim = current_sim.append(current_month)
    sim_dict[sim] = current_sim['ghi']

sdewes = pd.DataFrame(sim_dict)

sdewes.to_csv('/home/julian/Documentos/sdewes_2022_ghi')
