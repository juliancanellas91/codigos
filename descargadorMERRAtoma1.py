#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 15:50:57 2019

@author: julian
"""

import datetime

#####Editar estos datos a piaccere#####

#Los datos de MERRA van de 01 01 1980 - 30 06 2019. Completar con enteros.

AÑO_INICIO = 2015

MES_INICIO = 2

DIA_INICIO = 28

AÑO_FINAL = 2015

MES_FINAL = 4

DIA_FINAL = 1

# Lat y lon completar en sentido sur -> norte y oeste -> este

LAT_INICIAL =   -40.1243

LON_INICIAL = -65

LAT_FINAL = -30.124235435643655477

LON_FINAL = -55.

######################################

#Siempre empiezo en el primer dato de un día y termino en el último. 

inicio = datetime.datetime(AÑO_INICIO,MES_INICIO,DIA_INICIO, 00,30) # Las horas y minutos acá no aportan nada

inicio = inicio.toordinal() + 49/48 

final = datetime.datetime(AÑO_FINAL,MES_FINAL,DIA_FINAL, 23, 30)    # idem

final = final.toordinal() + 95/48

#La función datetime.toordinal me devuelve el dato que quiero pero parece no 
#estar de acuerdo con la convención adoptada en MERRA. 
#Yo utilicé datetime para convertir 01 01 1980 a ordinal y me da 722815. 
#MERRA asigna el valor 722816 a ese dato


# Hasta acá tengo los números que quiero. Ahora debería armar un loop que me busque el índice que corresponde.

from pydap.client import open_url
from pydap.cas.urs import setup_session

username= 'juliancanellas'
password= '49242964Gollo'
url='https://goldsmr4.gesdisc.eosdis.nasa.gov/dods/M2T1NXRAD'

session = setup_session(username=username,password=password,check_url=url)
RADdataset = open_url(url, session=session)

time = RADdataset['time']
time = time[:] #Este es el paso de descarga de los datos, toma mucho tiempo.
i = 0
while time[i] != inicio:
    i = i+1
print("el índice de tiempo para la fecha de inicio es:", i)

tiempoinicial = i

i = 0
while time[i] != final:
    i = i+1
print("el índice de tiempo para la fecha final es:", i)

tiempofinal = i

#listo
##########################
# Ahora lo mismo pero para lat y lon
lon = RADdataset['lon']
lonvect = lon[:] #lento
lat = RADdataset['lat']
latvect = lat[:] #lento


import math

def ubicacion(lat,long):
	ubicacion = []
	for i in range (len(lat)):
		for j in range (len(long)):
			punto= (lat[i],long[j])
			ubicacion.append(punto)

	return ubicacion

def distanciaMinima(a,b): 
	dmin = 0
	for i in range (0,(len(a))):
		d=math.sqrt(((a[i][0]-b[0])**2)+((a[i][1]-b[1])**2))
		if dmin == 0:
			dmin = d
		if d <= dmin:
			dmin = d
			c = a[i]
	return c

def puntoMasCercano (lat,long,a): #No es cierto que esta función devuelva el punto más cercano.
	u= ubicacion(lat,long)
	c= distanciaMinima(u,a)
	for i in range (0,len(lat)):
		if lat[i] == c[0]:
			x=i
	for j in range (0,len(long)):
		if long[j] == c[1]:
			y=j

	return x,y

mipunto = (LAT_INICIAL, LON_INICIAL) # SIEMPRE pondré latitud primero y longitud después.

idlat,idlon = puntoMasCercano(latvect,lonvect,mipunto) # Estos números que obtuve son los números de grilla

latinicial = idlat
loninicial = idlon

print( "Esquina suroeste del dominio:", lat[idlat],lon[idlon]) 

mipunto = (LAT_FINAL, LON_FINAL) # SIEMPRE pondré latitud primero y longitud después.

idlat,idlon = puntoMasCercano(latvect,lonvect,mipunto)

latfinal = idlat
lonfinal = idlon

print( "Esquina noreste del dominio:", lat[idlat],lon[idlon]) 


#listo
###################################

#Ahora, ¡A descargar!

swgdn = RADdataset['swgdn'] #Esto es GHI (creeeeeeeo)

GHI = swgdn[tiempoinicial:tiempofinal,latinicial:latfinal,loninicial:lonfinal] #Suuuuuper lento

# Esta variable GHI contiene 4 campos:
# swgdn tiene mis matrices por paso de tiempo.
# time tiene un vector con el tiempo en ordinal
# lat es vector latitud
# lon es vector longitud


# A partir de acá, debería armar distintas cosas según lo que quiero.
# Para el proyecto falopa, haría un csv con los datos por punto de grilla
# Para el solar, haría un conversor de BaseType a netCDF












































