#!/bin/bash

WRFbuild="$1" #/home/aotero/WRFintel_old

inRun='aerosol.formatted aerosol_lat.formatted aerosol_lon.formatted aerosol_plev.formatted bulkdens.asc_s_0_03_0_9 bulkradii.asc_s_0_03_0_9 CAM_ABS_DATA CAM_AEROPT_DATA CAMtr_volume_mixing_ratio.RCP8.5 CAMtr_volume_mixing_ratio.A1B CAMtr_volume_mixing_ratio.A2 CAMtr_volume_mixing_ratio.RCP4.5 CAMtr_volume_mixing_ratio.RCP6 capacity.asc CCN_ACTIVATE.BIN CLM_ALB_ICE_DFS_DATA CLM_ALB_ICE_DRC_DATA CLM_ASM_ICE_DFS_DATA CLM_ASM_ICE_DRC_DATA CLM_DRDSDT0_DATA CLM_EXT_ICE_DFS_DATA CLM_EXT_ICE_DRC_DATA CLM_KAPPA_DATA CLM_TAU_DATA coeff_p.asc coeff_q.asc constants.asc ETAMPNEW_DATA ETAMPNEW_DATA.expanded_rain GENPARM.TBL grib2map.tbl gribmap.txt kernels.asc_s_0_03_0_9 kernels_z.asc LANDUSE.TBL masses.asc MPTABLE.TBL ozone.formatted ozone_lat.formatted ozone_plev.formatted README.namelist RRTM_DATA RRTMG_LW_DATA RRTMG_SW_DATA SOILPARM.TBL termvels.asc tr49t67 tr49t85 tr67t85 URBPARM.TBL VEGPARM.TBL'

inMain='ndown.exe real.exe tc.exe wrf.exe'

#inWPS='met_em.d01.2012-05-02_00:00:00.nc met_em.d01.2012-05-02_03:00:00.nc met_em.d01.2012-05-02_06:00:00.nc met_em.d01.2012-05-02_09:00:00.nc met_em.d01.2012-05-02_12:00:00.nc met_em.d01.2012-05-02_15:00:00.nc met_em.d01.2012-05-02_18:00:00.nc met_em.d01.2012-05-02_21:00:00.nc met_em.d01.2012-05-03_00:00:00.nc'

for i in $inRun; do
    ln -s  $WRFbuild/WRFV3/run/$i
done

for i in $inMain; do
    ln -s  $WRFbuild/WRFV3/main/$i
done

#for i in $inWPS; do
#    ln -s  $WRFbuild/WPS/$i
#done

