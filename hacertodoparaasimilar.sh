#!bin/bash -x

cd /home/julian/Documentos/Build_WRF

ulimit -s unlimited
DIR="/home/julian/Documentos/Build_WRF/Libraries4"; export DIR
CC="gcc"; export CC
CXX="g++"; export CXX
FC="gfortran"; export FC
FCFLAGS="-m64"; export FCFLAGS
F77="gfortran"; export F77
FFLAGS="-m64"; export FFLAGS
PATH="$DIR/netcdf/bin:$PATH"; export PATH
NETCDF="$DIR/netcdf"; export NETCDF
PATH="$DIR/mpich/bin:$PATH"; export PATH
JASPERLIB="$DIR/grib2/lib"; export JASPERLIB
JASPERINC="$DIR/grib2/include"; export JASPERINC
WRFIO_NCD_LARGE_FILE_SUPPORT="1"; export WRFIO_NCD_LARGE_FILE_SUPPORT
LD_LIBRARY_PATH="$DIR/grib2/lib:$LD_LIBRARY_PATH"; export LD_LIBRARY_PATH
WRFPLUS_DIR="/home/julian/Documentos/Build_WRF/WRFPLUS"; export WRFPLUS_DIR
WRFDA_DIR="/home/julian/Documentos/Build_WRF/WRFDA"; export WRFDA_DIR
WORK_DIR="/home/julian/Documentos/Build_WRF/WRFDA/work_dir"; export WORK_DIR
DAT_DIR="/home/julian/Documentos/Build_WRF/DATA/TEST_DA/WRFDAV3.9-testdata"; export DAT_DIR

cd WPS 
rm -r met_em*
rm -r FILE*
rm -r geo_em*
./ungrib.exe
./geogrid.exe
./metgrid.exe

cd ../WRF/test/em_real

ln -sf ../../../WPS/met_em* . 
rm -r wrfi*
rm -r wrfb*
rm -r rsl.*
rm -r wrfo*
./real.exe

cp wrfinput_d01 ../../../WRFDA/work_dir12/fg

cd ../../../WRFDA/work_dir12

./da_wrfvar.exe




