# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""

import matplotlib.pyplot as plt
import datetime
import pandas as pd
import numpy as np
import netCDF4 as nc
import glob

#filelist = glob.glob('wrfout_d01_2017-12-1*')
#con este llamado levando todos los archivos del tercer dominio únicamente. TIENE LA FECHA PARA LA SEGUNDA SEMANA DE DICIEMBRE OJO CON ESO
#vm = nc.Dataset(filelist[3])

vm = nc.MFDataset(''*_0029')

latitude = -34.557
longitude = -58.505
# La idea es tener un archivo netCDF abierto como vm para extraerle las variables de radiación en el
#punto de grilla más cercano a la ubicación deseada, que también hay que definir antes.Las latitudes
#las definí con muchas cifras para garantizar que agarre bien cerca de Bs As.
#iLat = 97 #SIEMPRE CHEQUEAR ESTOS PUNTOS
#iLon = 94

data = vm

minLat = data.variables['XLAT'][0, :, :].min()
maxLat = data.variables['XLAT'][0, :, :].max()
minLon = data.variables['XLONG'][0, :, :].min()
maxLon = data.variables['XLONG'][0, :, :].max()

if not ((minLat < latitude < maxLat) and
                (minLon < longitude < maxLon)):
            print("Lat/Long out of file bounds")

gridDist = (data.variables['XLAT'][0, :, :] - latitude)**2 +\
            (data.variables['XLONG'][0, :, :] - longitude)**2

iLat, iLon = np.unravel_index(np.argmin(gridDist, axis=None),
                                      gridDist.shape)
#acá empieza el programa


time = []
for t in data.variables['Times'][:]:
            a = t.tostring().decode().split('_')
            c = a[0].split('-') + a[1].split(':')
            time.append(datetime.datetime(*list(map(int, c))))
timepd = pd.DatetimeIndex(pd.Series(time))
data_dict = {}
data_dict['ghi'] = data.variables['SWDOWN'][:, iLat, iLon]
data_dict['dni'] = data.variables['SWDDNI'][:, iLat, iLon]
data_dict['T 2m'] = data.variables['T2'][:, iLat, iLon]
data_dict['U 10m'] = data.variables['U10'][:, iLat, iLon]
data_dict['V 10m'] = data.variables['V10'][:, iLat, iLon]
data_dict
data_dict.keys()
pddata = pd.DataFrame(data_dict, index=timepd)
pddata
pddata.to_csv('ExtraídasRadiaciónMP2BL2SF2 CABA.csv')
