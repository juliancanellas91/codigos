#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 12:15:18 2021

@author: julian
"""

import numpy as np
import xarray as xr
import netCDF4 as nc
import matplotlib.pyplot as plt
from minisom import MiniSom
from scipy.cluster.vq import kmeans
from scipy.stats import ks_2samp
import sys
import os
import math
import pandas as pd
import numpy as np
import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

plt.rcParams["figure.figsize"] = (20,10)

from pvlib import clearsky
from pvlib.location import Location


ds = xr.load_dataset( '../adaptor.mars.internal-1634142789.2861948-17574-5-9021b7c0-49c4-49d9-a589-a92ed4700d1c.grib', engine='cfgrib')
z500 = ds['z'][:,1,:,:]
z900 = ds['z'][:,0,:,:]
neuron_len = 10
lat =  41
lon = 61
num_iteration = len(data)
sigma = 9
learning_rate = 0.005   
times = 1

z_trial = np.concatenate((z900,z500),axis = 1)
data = z_trial
data = np.nan_to_num(data)


k_means, error = kmeans(data,neuron_len**2)


som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
for i in range(neuron_len**2):
    som._weights[i//neuron_len][i%neuron_len] = k_means[i]
    
som.train(data, num_iteration*times)
weights = som._weights  

sigma = 2
learning_rate = 0.001
times = 10
som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
som._weights = weights

som.train(data, num_iteration*times)


peso900 = np.zeros((10,10,2501))
for i in range(10):
    for j in range(10):
        peso900[i,j,:] = som._weights[i,j,:2501]
        
peso500 = np.zeros((10,10,2501))
for i in range(10):
    for j in range(10):
        peso500[i,j,:] = som._weights[i,j,2501:]
 
        
fig, axs = plt.subplots(neuron_len, neuron_len, figsize=(20, 20))

pesos = np.zeros((neuron_len,neuron_len,lat,lon))

for i in range(len(pesos)):
    for j in range(len(pesos[0])):
        k = 0
        while k < len(peso900[0,0]):
            pesos[i][j][k//lon][k%lon]= peso900[i][j][k]
            k += 1

for i in range(0, neuron_len):
    for j in range(0, neuron_len):
        axs[i][j].contour(pesos[i][j],colors='black')
        
pesos = np.zeros((neuron_len,neuron_len,lat,lon))

for i in range(len(pesos)):
    for j in range(len(pesos[0])):
        k = 0
        while k < len(peso500[0][0]):
            pesos[i][j][k//lon][k%lon]=peso500[i][j][k]
            k += 1

for i in range(0, neuron_len):
    for j in range(0, neuron_len):
        axs[i][j].contour(pesos[i][j],colors='red')
        
plt.show()      


ds1 = xr.load_dataset('adaptor.mars.internal-1635349251.5622709-6033-7-d60b2ee4-eda8-4023-a17a-8e39821dfc1e.grib',engine='cfgrib')
ds2 = xr.load_dataset('adaptor.mars.internal-1635352295.817682-5387-5-93334a3f-f0ee-4eb7-aa80-73692e47cbce.grib', engine='cfgrib')

z900_1 = ds1['z'][:,0,:,:]
z500_1 = ds1['z'][:,2,:,:]
rh700_1 = ds1['r'][:,1,:,:]
z900_2 = ds2['z'][:,0,:,:]
z500_2 = ds2['z'][:,2,:,:]
rh700_2 = ds2['r'][:,1,:,:]

z900 = np.concatenate((z900_1,z900_2))
z500 = np.concatenate((z500_1,z500_2))
rh700 = np.concatenate((rh700_1,rh700_2))

z900 = np.nan_to_num(z900)
z500 = np.nan_to_num(z500)
rh700 = np.nan_to_num(rh700)

z900 = np.reshape(z900,(35064,41*61))
z500 = np.reshape(z500,(35064,41*61))
rh700 = np.reshape(rh700,(35064,41*61))

z900 = (z900-np.mean(z900))/np.std(z900)
z500 = (z500-np.mean(z500))/np.std(z500)
rh700 = (rh700-np.mean(rh700))/np.std(rh700)

data = np.concatenate((z900,z500),axis=1)
data = np.concatenate((data,rh700),axis = 1)

