
import os
import sys
import pdb
import glob
import subprocess
import calendar
import math
import datetime
import time



idate = (2017,12,18,0,0,0)
interval = 6.0 
fdate = (2017,12,25,0,0,0)
init_data = 'gfs'
pathtoinitdata = './ ' # Directorio para guardar los archivos

def download_data(date,initdata,pathtoinitdata):
    if initdata == 'gfs':
        prefix = 'gfsanl'
        n = '4'
        suffix = '.grb2'
    elif initdata == 'nam':
        prefix = 'namanl'
        n = '218'
        suffix = '.grb2'
    command = ('wget "https://www.ncei.noaa.gov/data/global-forecast-system/access/historical/analysis/' + date[:6] + '/' + date[:8] +
 '/' + prefix + '_' + n + '_' + date + '_000' + suffix + '" -P ' + pathtoinitdata)
#Para fechas posteriores a 2020 hay que cambiar la url por https://www.ncei.noaa.gov/data/global-forecast-system/access/grid-004-0.5-degree/analysis/
#debería poner algo así como if date[0] <= 2019 then else etc etc También tendría que ver cómo incluir los pronósticos a 3 horas
    os.system(command)
    return

def get_init_files(initdata,idate,interval,fdate,pathtoinitdata):
    if initdata=='gfs':
        prefix = 'gfsanl'
    elif initdata=='nam':
        prefix = 'namanl'

    idaten = calendar.timegm(idate)
    fdaten = calendar.timegm(fdate)

    ifiledate = idaten - idaten%(interval*3600)
    ffiledate = fdaten - fdaten%(interval*3600) + (interval*3600)

    required_dates = range(int(ifiledate),int(ffiledate),int(interval*3600))

    initfiles = glob.glob(pathtoinitdata + '*')

    for r in required_dates:
        longdate = time.gmtime(r)
        fname_date = ''.join(["%02u" %x for x in longdate[0:3]]) + '_' + ''.join(["%02u" %x for x in longdate[3:5]])
        checkfiles_prefix = []
        checkfiles_date = []
        for f in initfiles:
            checkfiles_prefix.append(prefix in f)
            checkfiles_date.append(fname_date in f)
        try:
            checkfiles_prefix.index(1) + checkfiles_date.index(1)
        except ValueError:
            print ('Downloading required file for ' + fname_date)
            download_data(fname_date,init_data,pathtoinitdata)
        else:
            print ('Data for ' + fname_date + ' already exists.')
    return

get_init_files(init_data,idate,interval,fdate,pathtoinitdata)

