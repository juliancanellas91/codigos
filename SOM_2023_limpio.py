"""
Dependencies
"""

import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
import matplotlib.cm as cm
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from minisom import MiniSom
from scipy.cluster.vq import kmeans
from scipy.stats import ks_2samp
import pandas as pd
from minisom import MiniSom

"""
Input parameters
"""

path = './'
rad_file = 'radiation_hourly.csv'
var1_file = 'era5-2015-2020-z700-horario-30406353.grib'
var2_file = 'era5-2015-2020-tclw-horario-30406353.grib'

# Names as they appear in the reanalysis databases
var1_name = 'z'
var2_name = 'tclw'

# Leves just for labelling purposes
level1 = '700'
level2 = 'sfc'


neuron_len = 10
lat = 41 #As in reanalysis database
lon = 41
rows = neuron_len
columns = neuron_len
num_iteration = 52608 # Time steps must be equal both in reanalysis and radiation databases
sigma = neuron_len - 1
learning_rate = 0.005
times = 1


rad_proc = pd.read_csv(path+rad_file)
ds    = xr.load_dataset(var1_file, engine = 'cfgrib')
ds2  = xr.load_dataset(var2_file, engine = 'cfgrib')

"""
Convert fields to SOM input vectors
"""

var1 = np.array(ds[var1_name])
var2 = np.array(ds2[var2_name])

var1 = np.reshape(var1,(num_iteration,lat*lon))
var2 = np.reshape(var2,(num_iteration,lat*lon))

var1 = np.nan_to_num(var1)
var2 = np.nan_to_num(var2)

average1 = np.zeros((len(var1[0])))
for i in range(len(average1)):
    average1[i] = np.mean(var1[:,i])
anom1 = np.zeros(var1.shape)
for i in range(len(var1)):
    anom1[i,:] = var1[i,:] - average1
std1 = np.zeros((len(var1[0])))
for i in range(len(std1)):
    std1[i] = np.std(var1[:,i])

average2 = np.zeros((len(var2[0])))
for i in range(len(average2)):
    average2[i] = np.mean(var2[:,i])
anom2 = np.zeros(var2.shape)
for i in range(len(var2)):
    anom2[i,:] = var2[i,:] - average2
std2 = np.zeros((len(var2[0])))
for i in range(len(std2)):
    std2[i] = np.std(var2[:,i])

data = np.concatenate(np.array([anom1/std1,anom2/std2]),axis=1)


"""
Begin training, initialized with kmean seeds.
There are two steps in the training.
First, a "coarse" training,
followed by a "fine tunning" training
"""

k_means, error = kmeans(data,neuron_len**2)

#Coarse
som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
for i in range(neuron_len**2):
  som._weights[i//neuron_len][i%neuron_len] = k_means[i]
som.train(data, int(num_iteration*times))
weights = som._weights

#Fine tune
sigma = 2
learning_rate = 0.01
times = 10
som = MiniSom(neuron_len, neuron_len, len(data[0]), sigma, learning_rate)
som._weights = weights
som.train(data, int(num_iteration*times))

"""
Prepare trained data for visualization
"""

weights = som._weights
weights1 = np.zeros((neuron_len,neuron_len,lat*lon))
weights2 = np.zeros((neuron_len,neuron_len,lat*lon))
for i in range(len(weights)):
  for j in range(len(weights[0])):
    weights1[i][j] = weights[i][j][:(lat*lon)]
    weights2[i][j] = weights[i][j][(lat*lon):]

def return_to_latlon(weights):
  pesos = np.zeros((neuron_len,neuron_len,lat,lon))
  for i in range(len(pesos)):
    for j in range(len(pesos[0])):
        k = 0
        while k < len(weights[0][0]):
          pesos[i][j][k//lon][k%lon]=weights[i][j][k]
          k += 1
  return pesos

for i in range(neuron_len):
  for j in range(neuron_len):
    weights1[i][j] = weights1[i][j] * std1 + average1
    weights2[i][j] = weights2[i][j] * std2 + average2

pesos1 = return_to_latlon(weights1)
pesos2 = return_to_latlon(weights2)


daytime_winners = np.zeros((neuron_len,neuron_len))
for instant in range(len(data)):
    if rad_proc['day'][instant]:
        daytime_winners[som.winner(data[instant])] += 1
total_winners = som.activation_response(data)

highly_variable = rad_proc.loc[rad_proc['vi'] >5] #Hardcoded variability threshold
c = np.zeros((neuron_len,neuron_len))
for variable_instant in highly_variable.index:
    c[som.winner(data[variable_instant])] +=1

variable_as_percentage = np.zeros((neuron_len,neuron_len))
for i in range(neuron_len):
  for j in range(neuron_len):
    variable_as_percentage[i][j] =c[i][j]/daytime_winners[i][j] * 100

aver = som.win_map(data,return_indices=True)
rad_proc = rad_proc.fillna(0)
average_vi = np.zeros((neuron_len,neuron_len))

for i in range(neuron_len):
  for j in range(neuron_len):
    vi = []
    for index in aver[(i,j)]:
      vi.append(rad_proc['vi'][index])
    average_vi[i,j] = np.mean(vi)

"""
Plot:
 1- The trained fields
 2- "activation response", or the frequency count per node
 3- The percentage of the later that are classified as "variable"
 4- The average variability score per node
"""


#1
fig, axs = plt.subplots(neuron_len, neuron_len, figsize=(15, 15))
for i in range(neuron_len):
    for j in range(neuron_len):
        axs[i][j].imshow(pesos2[i][j],cmap='Greys')
        axs[i][j].contour(pesos1[i][j],cmap='Reds')
#plt.show()
plt.savefig(var1_name+level1+var2_name+level2+'.png')
plt.close()

#2
# fig, axs = plt.subplots(4, 1, figsize=(15, 15))
cmap=cm.get_cmap('viridis')
#normalizer=Normalize(0,1500)
#im=cm.ScalarMappable(norm=normalizer)
plt.imshow(daytime_winners,cmap=cmap)#,norm=normalizer)
plt.title('Horas diurnas')
plt.colorbar()#im)
#plt.show()
plt.savefig(var1_name+level1+var2_name+level2+'_activationresponse.png')
plt.close()

#3
cmap=cm.get_cmap('viridis')
#normalizer=Normalize(10,40)
#im=cm.ScalarMappable(norm=normalizer)
plt.imshow(variable_as_percentage,cmap=cmap)#,norm=normalizer)
plt.title('Horas variables como porcentaje de las horas diurnas')
plt.colorbar()#im)
#plt.show()
plt.savefig(var1_name+level1+var2_name+level2+'_variableaspercentage.png')
plt.close()

#4
plt.imshow(average_vi)
plt.colorbar()
#plt.show()
plt.savefig(var1_name+level1+var2_name+level2+'_vipromedio.png')
plt.close()
