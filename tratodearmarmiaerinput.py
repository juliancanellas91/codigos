#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 31 12:48:47 2019

@author: julian
"""

import xarray 

wrfinput_d01 = xarray.open_dataset('wrfinput_d01')
 
aerinput_d01 = xarray.open_dataset('aerinput_d01')

varlist = []

for var, dr in aerinput_d01.data_vars.items():
    print(var, end=',')
    varlist.append(var)


#Tengo que crear un data array tomando los values de mi aerinput, renombrandole las dimensiones y las coordenadas. 
    
#"AOD5502D" <----- TOTEXTTAU
#"ANGEXP2D" <----- TOTANGSTR
#"AERSSA2D" <----  TOTSCATAU

# 1) Creo un data array para cada una 
    
AOD5502D = xarray.DataArray(aerinput_d01['TOTEXTTAU'].values, coords=[aerinput_d01['time'].values, wrfinput_d01['XLAT'][0,:,0].values, wrfinput_d01['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )
ANGEXP2D = xarray.DataArray(aerinput_d01['TOTANGSTR'].values, coords=[aerinput_d01['time'].values, wrfinput_d01['XLAT'][0,:,0].values, wrfinput_d01['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )
AERSSA2D = xarray.DataArray(aerinput_d01['TOTSCATAU'].values, coords=[aerinput_d01['time'].values, wrfinput_d01['XLAT'][0,:,0].values, wrfinput_d01['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )

# 2) Meter este data array en un dataset con los atributos correctos.

aerinput_d01 = xarray.Dataset({'AOD5502D':AOD5502D,'ANGEXP2D':ANGEXP2D,'AERSSA2D':AERSSA2D})
aerinput_d01 = aerinput_d01.assign_attrs(wrfinput_d01.attrs)

# 3) Guardar!

aerinput_d01.to_netcdf('aerinput_d01.nc')   # No estoy seguro de si debería guardar como nc o nc4. Hice los dos, y sus propiedades parecen distintas
#####################SUCCESSSSSSSSSSSSS#########################

#repito:

wrfinput_d02 = xarray.open_dataset('wrfinput_d02')
aerinput_d02 = xarray.open_dataset('aerinput_d02')

AOD5502D = xarray.DataArray(aerinput_d02['TOTEXTTAU'].values, coords=[aerinput_d02['time'].values, wrfinput_d02['XLAT'][0,:,0].values, wrfinput_d02['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )
ANGEXP2D = xarray.DataArray(aerinput_d02['TOTANGSTR'].values, coords=[aerinput_d02['time'].values, wrfinput_d02['XLAT'][0,:,0].values, wrfinput_d02['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )
AERSSA2D = xarray.DataArray(aerinput_d02['TOTSCATAU'].values, coords=[aerinput_d02['time'].values, wrfinput_d02['XLAT'][0,:,0].values, wrfinput_d02['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )

aerinput_d02 = xarray.Dataset({'AOD5502D':AOD5502D,'ANGEXP2D':ANGEXP2D,'AERSSA2D':AERSSA2D})
aerinput_d02 = aerinput_d02.assign_attrs(wrfinput_d02.attrs)

aerinput_d02.to_netcdf('aerinput_d02.nc')

wrfinput_d03 = xarray.open_dataset('wrfinput_d03')
aerinput_d03 = xarray.open_dataset('aerinput_d03')

AOD5502D = xarray.DataArray(aerinput_d03['TOTEXTTAU'].values, coords=[aerinput_d03['time'].values, wrfinput_d03['XLAT'][0,:,0].values, wrfinput_d03['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )
ANGEXP2D = xarray.DataArray(aerinput_d03['TOTANGSTR'].values, coords=[aerinput_d03['time'].values, wrfinput_d03['XLAT'][0,:,0].values, wrfinput_d03['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )
AERSSA2D = xarray.DataArray(aerinput_d03['TOTSCATAU'].values, coords=[aerinput_d03['time'].values, wrfinput_d03['XLAT'][0,:,0].values, wrfinput_d03['XLONG'][0,0,:].values], dims=('Time','south_north','west_east') )

aerinput_d03 = xarray.Dataset({'AOD5502D':AOD5502D,'ANGEXP2D':ANGEXP2D,'AERSSA2D':AERSSA2D})
aerinput_d03 = aerinput_d03.assign_attrs(wrfinput_d03.attrs)

aerinput_d03.to_netcdf('aerinput_d03.nc')
